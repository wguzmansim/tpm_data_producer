﻿using System;
using System.Configuration;
using System.IO;
using System.IO.Compression;

namespace TPMArchiver
{
    class Program
    {
        static void Main(string[] args)
        {
            //Date variables
            string CurrentYear = DateTime.Now.ToString("yyyy");
            string CurrentMonth = DateTime.Now.ToString("MM");
            string CurrentDay = DateTime.Now.ToString("dd");

            //CSV data backup
            string CSVFolderPath = ConfigurationManager.AppSettings["CSVFolder"];
            string CurrentMonthCSVBackupFolder = ConfigurationManager.AppSettings["BackupFolder"] + Path.DirectorySeparatorChar + "CSVs" + Path.DirectorySeparatorChar + CurrentYear + Path.DirectorySeparatorChar + CurrentMonth;
            System.IO.Directory.CreateDirectory(CurrentMonthCSVBackupFolder);
            string CSVZipPath = CurrentMonthCSVBackupFolder + Path.DirectorySeparatorChar + CurrentDay + ".zip";

            try
            {
                //Creates Zip backup file
                ZipFile.CreateFromDirectory(CSVFolderPath, CSVZipPath);

                //Delete current files in CSV folder
                System.IO.DirectoryInfo di = new DirectoryInfo(CSVFolderPath);

                foreach (FileInfo file in di.GetFiles())
                {
                    file.Delete();
                }
                Console.WriteLine("Data zip created");
            }
            catch (Exception e)
            {
                Console.WriteLine("Error message = " + e.Message);
                Environment.Exit(0);
            }

            //CSV data backup
            string ErrorsFolderPath = ConfigurationManager.AppSettings["ErrorsFolder"];
            string CurrentMonthErrorsBackupFolder = ConfigurationManager.AppSettings["BackupFolder"] + Path.DirectorySeparatorChar + "Error Logs" + Path.DirectorySeparatorChar + CurrentYear + Path.DirectorySeparatorChar + CurrentMonth;
            System.IO.Directory.CreateDirectory(CurrentMonthErrorsBackupFolder);
            string ErrorsZipPath = CurrentMonthErrorsBackupFolder + Path.DirectorySeparatorChar + CurrentDay + ".zip";

            try
            {
                //Creates Zip backup file
                ZipFile.CreateFromDirectory(ErrorsFolderPath, ErrorsZipPath);

                //Delete current files in Errors folder
                System.IO.DirectoryInfo di = new DirectoryInfo(ErrorsFolderPath);

                foreach (FileInfo file in di.GetFiles())
                {
                    file.Delete();
                }
                Console.WriteLine("Errors zip created");
            }
            catch (Exception e)
            {
                Console.WriteLine("Error message = " + e.Message);
                Environment.Exit(0);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TPMConfigUpdater
{
    public class ConfigUpdater
    {
        public void Start()
        {
            var starterConfigFile = ConfigurationManager.OpenExeConfiguration(ConfigurationManager.AppSettings["StarterExeFile"]);
            starterConfigFile.AppSettings.Settings[ConfigurationManager.AppSettings["StartDateSetting"]].Value = DateTime.Now.AddDays(int.Parse(ConfigurationManager.AppSettings["DateRange"]) * -1).ToString("yyyy-MM-dd");
            starterConfigFile.AppSettings.Settings[ConfigurationManager.AppSettings["EndDateSetting"]].Value = DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd");
            starterConfigFile.Save();
        }
    }
}

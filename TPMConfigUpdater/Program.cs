﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TPMConfigUpdater
{
    class Program
    {
        static void Main(string[] args)
        {
            var cfgUpdater = new ConfigUpdater();
            cfgUpdater.Start();
        }
    }
}

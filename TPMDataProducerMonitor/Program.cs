﻿using System;
using System.Configuration;
using System.Net;
using TPMLog;
using VelocityTracer;

namespace TPMDataProducerMonitor
{
    class Program
    {
        static int LogLevel = int.Parse(ConfigurationManager.AppSettings["LogLevel"]);
        public static Tracer Tracer = new Tracer("TPMMonitor");

        static void Main(string[] args)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;

            string LogMessage = String.Format("TPMMonitor started at {0}", DateTime.Now.ToString());

            //CloudWatch log message
            Tracer.AddLogEvent(new TracerParameter(LogMessage, "", System.Environment.MachineName, "", "", false, -1));

            Log.LogMessage(LogMessage, ConfigurationManager.AppSettings["LogFileName"]);

            try
            {
                var DataProducerMonitor = new TPMDataProducerMonitor();
                DataProducerMonitor.RunMonitor(Tracer);
            }
            catch (Exception e)
            {
                string ErrorMessage = string.Format("Could not start monitor at" + DateTime.Now.ToString() + ", error = {0}", e.Message);
                Log.LogMessage(ErrorMessage, ConfigurationManager.AppSettings["LogFileErrorName"]);

                //CloudWatch log message
                if (LogLevel == 1 || LogLevel == 2)
                {
                    Tracer.AddLogEvent(new TracerParameter(ErrorMessage, "", System.Environment.MachineName, "", "", true, -1));
                    Tracer.Dispose();
                }

                return;
            }

            LogMessage = String.Format("TPMMonitor ended at {0}", DateTime.Now.ToString());
            Log.LogMessage(LogMessage, ConfigurationManager.AppSettings["LogFileName"]);

            //CloudWatch log message
            Tracer.AddLogEvent(new TracerParameter(LogMessage, "", System.Environment.MachineName, "", "", false, -1));
            Tracer.Dispose();
        }
    }
}

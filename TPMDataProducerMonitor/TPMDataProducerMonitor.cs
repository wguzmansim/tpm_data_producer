﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Data.Odbc;
using System.Diagnostics;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

using Amazon.Redshift;
using Amazon.SQS;
using Amazon.SQS.Model;
using Amazon.S3;

using TPMLog;
using TPMEntities;
using VelocityTracer;
using Amazon.S3.Model;

namespace TPMDataProducerMonitor
{
    class TPMDataProducerMonitor
    {
        private int MaxWorkers;
        private string TPMDataProducerQueue = ConfigurationManager.AppSettings["TPMDataProducerQueue"];
        private Stopwatch stopWatch = new Stopwatch();
        private String mainDbName = ConfigurationManager.AppSettings["RedshiftDBName"];
        private String endpointAddress = ConfigurationManager.AppSettings["RedshiftHost"];
        private String endpointPort = ConfigurationManager.AppSettings["RedshiftPort"];
        private string masterUsername = ConfigurationManager.AppSettings["RedshiftUsername"];
        private string password = ConfigurationManager.AppSettings["RedshiftPassword"];
        static int LogLevel = int.Parse(ConfigurationManager.AppSettings["LogLevel"]);
        public string LogMessage = "";
        public string ErrorMessage = "";

        private HybridDictionary Workers = new HybridDictionary();

        public void RunMonitor(Tracer Tracer)
        {
            try
            {
                string odbcConnectionString = string.Concat("Driver={PostgreSQL Unicode}; Server=", endpointAddress, "; Database=", mainDbName, "; UID=", masterUsername, "; PWD=", password, "; Port=", endpointPort);
                using (OdbcConnection conn = new OdbcConnection(odbcConnectionString))
                {
                    try
                    {
                        //Resets the ETL temporary tables first
                        conn.Open();
                        OdbcCommand TruncateETLTPMBulkload = new OdbcCommand("TRUNCATE TABLE etl.etl_tpm_temp_metric_data_bulkload;", conn);
                        TruncateETLTPMBulkload.ExecuteNonQuery();

                        OdbcCommand TruncateETLTPM = new OdbcCommand("TRUNCATE TABLE etl.etl_tpm_temp_metric_data;", conn);
                        TruncateETLTPM.ExecuteNonQuery();

                        OdbcCommand TruncateETLError = new OdbcCommand("TRUNCATE TABLE etl.temp_data_producer_error_log;", conn);
                        TruncateETLError.ExecuteNonQuery();
                    }
                    catch (AmazonRedshiftException e)
                    {
                        if (e.ErrorCode != null &&
                            (e.ErrorCode.Equals("InvalidAccessKeyId")
                            ||
                            e.ErrorCode.Equals("InvalidSecurity")))
                        {
                            ErrorMessage = string.Format("TPMMonitor error when running the ETL. Check the provided AWS Credentials. {0}", e);
                            Log.LogMessage(ErrorMessage, ConfigurationManager.AppSettings["LogFileErrorName"]);

                            //CloudWatch log message
                            if (LogLevel == 1 || LogLevel == 2)
                            {
                                Tracer.AddLogEvent(new TracerParameter(ErrorMessage, "", System.Environment.MachineName, "", "", true, -1));
                            }
                        }
                        else
                        {
                            ErrorMessage = string.Format("TPMMonitor error occurred. Message:'{0}' when truncating ETL temporary table", e.Message);
                            Log.LogMessage(ErrorMessage, ConfigurationManager.AppSettings["LogFileErrorName"]);

                            //TODO: Log error message to be sent to Redshift

                            //CloudWatch log message
                            if (LogLevel == 1 || LogLevel == 2)
                            {
                                Tracer.AddLogEvent(new TracerParameter(ErrorMessage, "", System.Environment.MachineName, "", "", true, -1));
                            }
                        }
                    }
                }

                MaxWorkers = int.Parse(ConfigurationManager.AppSettings["MaxWorkers"]);

                //Starts stopwatch
                stopWatch.Restart();

                MonitorQueue(Tracer);
            }
            catch (Exception e)
            {
                ErrorMessage = string.Format("Could not start monitor queue, error = {0}", e.Message);

                Log.LogMessage(ErrorMessage, ConfigurationManager.AppSettings["LogFileErrorName"]);

                //CloudWatch log message
                if (LogLevel == 1 || LogLevel == 2)
                {
                    Tracer.AddLogEvent(new TracerParameter(ErrorMessage, "", System.Environment.MachineName, "", "", true, -1));
                }
                return;
            }
        }

        private void MonitorQueue(Tracer Tracer)
        {
            while (true)
            {
                var AvailableMessages = GetMessages(1);

                //Checks if no more messages are available AND max time has already elapsed AND no workers are running
                TimeSpan ts = stopWatch.Elapsed;
                if (this.GetTotalMessages() == 0 && ts.Minutes >= Int32.Parse(ConfigurationManager.AppSettings["MonitorMaxMinutesElapsed"]) && Workers.Count == 0)
                {
                    this.TurnOffMonitor(Tracer);
                }

                foreach (var Message in AvailableMessages)
                {
                    while (Workers.Count >= MaxWorkers)
                    {
                        System.Threading.Thread.Sleep(1000);
                    }
                    
                    System.Threading.Thread.Sleep(200);
                    string LogMessage = "Launching worker for message ID = " + Message.MessageId;
                    Log.LogMessage(string.Format(LogMessage), ConfigurationManager.AppSettings["LogFileName"]);

                    //CloudWatch log message
                    if (LogLevel == 2)
                    {
                        Tracer.AddLogEvent(new TracerParameter(LogMessage, "", System.Environment.MachineName, "", "", false, -1));
                    }
                    Console.WriteLine(LogMessage);

                    var WorkerID = Guid.NewGuid();
                    Workers.Add(WorkerID,
                    Task.Factory.StartNew(() => LaunchWorker(Message, Tracer),
                                                new System.Threading.CancellationToken(),
                                                TaskCreationOptions.None,
                                                TaskScheduler.Default).ContinueWith((antecedent) => Workers.Remove(WorkerID),
                                                new System.Threading.CancellationToken(),
                                                    TaskContinuationOptions.ExecuteSynchronously,
                                                    TaskScheduler.Default));
                }
            }
        }

        private void LaunchWorker(Amazon.SQS.Model.Message Message, Tracer Tracer)
        {
            try
            {
                Regex trimmer = new Regex(@"\s\s+");
                var WorkerArguments = trimmer.Replace(Message.Body, " ");
                string ArgumentsString = Message.Body.Replace("\"\"", "'");

                ArgumentsString = ArgumentsString.Replace(":',", ":\"\",");
                //ArgumentsString = ArgumentsString.Replace(@"""", @"\""");
                TPMQueueMessage QueueMessage = Newtonsoft.Json.JsonConvert.DeserializeObject<TPMQueueMessage>(ArgumentsString);
                var WorkerFilePath = "";
                switch (QueueMessage.ThirdPartyChannel)
                {
                    case TPMEntities.Enums.ThirdPartyChannel.GoogleAnalytics:
                        WorkerFilePath = ConfigurationManager.AppSettings["TPMWorkerGAExePath"];
                        WorkerArguments = WorkerArguments.Replace(";", ",");
                        break;
                    case TPMEntities.Enums.ThirdPartyChannel.GoogleMyBusiness:
                        WorkerFilePath = ConfigurationManager.AppSettings["TPMWorkerGMBExePath"];
                        WorkerArguments = WorkerArguments.Replace("\"", "\\\"");
                        break;
                    default:
                        //Throw an exception
                        break;
                }
                
                var appLaunchInfo = new System.Diagnostics.ProcessStartInfo()
                {
                    FileName = WorkerFilePath,
                    Arguments = WorkerArguments
                };

                LogMessage = string.Format("Launching worker {1} for message = {0} ", Message.MessageId, QueueMessage.ThirdPartyChannel);
                Log.LogMessage(LogMessage, ConfigurationManager.AppSettings["LogFileName"]);

                //CloudWatch log message
                if (LogLevel == 2)
                {
                    Tracer.AddLogEvent(new TracerParameter(LogMessage, "", System.Environment.MachineName, "", "", false, -1));
                }

                var worker = Process.Start(appLaunchInfo);
                worker.WaitForExit();
                var ExitCode = worker.ExitCode;
                LogMessage = string.Format("Finished working for message = {0} with Exit Code = {1}", Message.Body, ExitCode);
                Console.WriteLine(LogMessage);

                //CloudWatch log message
                if (LogLevel == 2)
                {
                    Tracer.AddLogEvent(new TracerParameter(LogMessage, "", System.Environment.MachineName, "", "", false, -1));
                }

                //Resets stopwatch
                stopWatch.Restart();

                if (ExitCode == 0)
                {
                    DeleteMessage(Message.ReceiptHandle);
                }
                else
                {
                    string ErrorMessage = "";
                    switch (ExitCode)
                    {
                        case 1:
                            ErrorMessage = string.Format("TPMWorkerGMB error at {0} for message ID = {1}, message body = {2}, Invalid arguments for TPMWorkerGMB for directory ID = {3}", DateTime.Now.ToString(), Message.MessageId, Message.Body, QueueMessage.DirectoryID);
                            DeleteMessage(Message.ReceiptHandle);
                            break;
                        case 2:
                            ErrorMessage = string.Format("TPMWorkerGA error at {0} for message ID = {1}, message body = {2}, An error occurred trying to generate Google Access Token for directory ID = {3}", DateTime.Now.ToString(), Message.MessageId, Message.Body, QueueMessage.DirectoryID);
                            DeleteMessage(Message.ReceiptHandle);
                            break;
                        case 3:
                            ErrorMessage = string.Format("TPMWorkerGMB error at {0} for message ID = {1}, message body = {2}, Error getting directory ID data for directory ID = {3}", DateTime.Now.ToString(), Message.MessageId, Message.Body, QueueMessage.DirectoryID);
                            DeleteMessage(Message.ReceiptHandle);
                            break;
                        case 5:
                            ErrorMessage = string.Format("TPMWorkerGMB error at {0} for message ID = {1}, message body = {2}, An error occurred trying to generate Google Access Token for directory ID = {3}", DateTime.Now.ToString(), Message.MessageId, Message.Body, QueueMessage.DirectoryID);
                            DeleteMessage(Message.ReceiptHandle);
                            break;
                        case 6:
                            ErrorMessage = string.Format("TPMWorkerGMB error at {0} for message ID = {1}, message body = {2}, Failed to create request for GMB API for directory ID = {3}", DateTime.Now.ToString(), Message.MessageId, Message.Body, QueueMessage.DirectoryID);
                            DeleteMessage(Message.ReceiptHandle);
                            break;
                        case 7:
                            ErrorMessage = string.Format("TPMWorkerGMB error at {0} for message ID = {1}, message body = {2}, Error in GMB API response for directory ID = {3}", DateTime.Now.ToString(), Message.MessageId, Message.Body, QueueMessage.DirectoryID);
                            DeleteMessage(Message.ReceiptHandle);
                            break;
                        case 8:
                            ErrorMessage = string.Format("TPMWorkerGMB error at {0} for message ID = {1}, message body = {2}, Deleted entity for directory ID = {3}", DateTime.Now.ToString(), Message.MessageId, Message.Body, QueueMessage.DirectoryID);
                            DeleteMessage(Message.ReceiptHandle);
                            break;
                        case 9:
                            ErrorMessage = string.Format("TPMWorkerGMB error at {0} for message ID = {1}, message body = {2}, Error creating the S3 bucket file for directory ID = {3}", DateTime.Now.ToString(), Message.MessageId, Message.Body, QueueMessage.DirectoryID);
                            DeleteMessage(Message.ReceiptHandle);
                            break;
                        case 10:
                            ErrorMessage = string.Format("TPMWorkerGA error at {0} for message ID = {1}, message body = {2}, Error getting directory data from Velocity API for directory ID = {3}", DateTime.Now.ToString(), Message.MessageId, Message.Body, QueueMessage.DirectoryID);
                            DeleteMessage(Message.ReceiptHandle);
                            break;
                        case 11:
                            ErrorMessage = string.Format("TPMWorkerGA error at {0} for message ID = {1}, message body = {2}, An error occurred getting access token for metrics list for directory ID = {3}", DateTime.Now.ToString(), Message.MessageId, Message.Body, QueueMessage.DirectoryID);
                            DeleteMessage(Message.ReceiptHandle);
                            break;
                        case 12:
                            ErrorMessage = string.Format("TPMWorkerGA error at {0} for message ID = {1}, message body = {2}, An error occurred getting access token for list of accounts for directory ID = {3}", DateTime.Now.ToString(), Message.MessageId, Message.Body, QueueMessage.DirectoryID);
                            DeleteMessage(Message.ReceiptHandle);
                            break;
                        case 13:
                            ErrorMessage = string.Format("TPMWorkerGA error at {0} for message ID = {1}, message body = {2}, An error occurred getting access token for account ID for directory ID = {3}", DateTime.Now.ToString(), Message.MessageId, Message.Body, QueueMessage.DirectoryID);
                            DeleteMessage(Message.ReceiptHandle);
                            break;
                        case 14:
                            ErrorMessage = string.Format("TPMWorkerGA error at {0} for message ID = {1}, message body = {2}, An error occurred getting profiles list for directory ID = {3}", DateTime.Now.ToString(), Message.MessageId, Message.Body, QueueMessage.DirectoryID);
                            DeleteMessage(Message.ReceiptHandle);
                            break;
                        case 15:
                            ErrorMessage = string.Format("TPMWorkerGA error at {0} for message ID = {1}, message body = {2}, An error occurred trying to generate Google Access Token for directory ID = {3}", DateTime.Now.ToString(), Message.MessageId, Message.Body, QueueMessage.DirectoryID);
                            DeleteMessage(Message.ReceiptHandle);
                            break;
                        case 16:
                            ErrorMessage = string.Format("TPMWorkerGA error at {0} for message ID = {1}, message body = {2}, Error creating the S3 bucket file for directory ID = {3}", DateTime.Now.ToString(), Message.MessageId, Message.Body, QueueMessage.DirectoryID);
                            DeleteMessage(Message.ReceiptHandle);
                            break;
                        case 17:
                            ErrorMessage = string.Format("TPMWorkerGA error at {0} for message ID = {1}, message body = {2}, The required metrics and dimensions were not specified. Metrics used = {0}, Dimensions used = {1} for directory ID = {3}", DateTime.Now.ToString(), Message.MessageId, Message.Body, QueueMessage.DirectoryID);
                            DeleteMessage(Message.ReceiptHandle);
                            break;
                        case 18:
                            ErrorMessage = string.Format("TPMWorkerGMB error at {0} for message ID = {1}, message body = {2}, Null response content for directory ID = {3}", DateTime.Now.ToString(), Message.MessageId, Message.Body, QueueMessage.DirectoryID);
                            DeleteMessage(Message.ReceiptHandle);
                            break;
                        case 19:
                            ErrorMessage = string.Format("TPMLog error at {0} for message ID = {1}, message body = {2}, Error loading error log file to Redshift directory ID = {3}", DateTime.Now.ToString(), Message.MessageId, Message.Body, QueueMessage.DirectoryID);
                            DeleteMessage(Message.ReceiptHandle);
                            break;
                        default:
                            ErrorMessage = string.Format("TPMWorker ({4}) error at {0} for message ID = {1}, message body = {2}, Unexpected error for directory ID = {3}, exit code = {5}", DateTime.Now.ToString(), Message.MessageId, Message.Body, QueueMessage.DirectoryID, QueueMessage.ThirdPartyChannel, ExitCode);
                            DeleteMessage(Message.ReceiptHandle);
                            break;
                    }
                    Log.LogMessage(ErrorMessage, ConfigurationManager.AppSettings["LogFileErrorName"]);

                    //TODO: Log error message to be sent to Redshift

                    //CloudWatch log message
                    if (LogLevel == 1 || LogLevel == 2)
                    {
                        Tracer.AddLogEvent(new TracerParameter(ErrorMessage, "", System.Environment.MachineName, "", "", true, -1));
                    }

                    Console.WriteLine(ErrorMessage);
                }
            }
            finally
            {
                //TODO: Append to error string log
            }

            LogMessage = "TPMDataProducer worker ended at" + DateTime.Now.ToString();
            Console.WriteLine(LogMessage);

            //CloudWatch log message
            if (LogLevel == 2)
            {
                Tracer.AddLogEvent(new TracerParameter(LogMessage, "", System.Environment.MachineName, "", "", false, -1));
            }
        }

        private List<Amazon.SQS.Model.Message> GetMessages(int MaxMessages)
        {
            var sqs = new AmazonSQSClient();
            var receiveMessageRequest = new ReceiveMessageRequest { QueueUrl = TPMDataProducerQueue, MaxNumberOfMessages = MaxMessages };
            var receiveMessageResponse = sqs.ReceiveMessage(receiveMessageRequest);

            return receiveMessageResponse.Messages;
        }

        private int GetTotalMessages()
        {
            var sqs = new AmazonSQSClient();
            var CurrentStatusQueue = sqs.GetQueueAttributes(TPMDataProducerQueue, new List<string>() { "ApproximateNumberOfMessages", "ApproximateNumberOfMessagesDelayed", "ApproximateNumberOfMessagesNotVisible" });

            int Total = CurrentStatusQueue.ApproximateNumberOfMessages + CurrentStatusQueue.ApproximateNumberOfMessagesDelayed + CurrentStatusQueue.ApproximateNumberOfMessagesNotVisible;

            return Total;
        }

        private void DeleteMessage(string ReceiptHandle)
        {
            var sqs = new AmazonSQSClient();
            var deleteMessageRequest = new DeleteMessageRequest { QueueUrl = TPMDataProducerQueue, ReceiptHandle = ReceiptHandle };
            var response = sqs.DeleteMessage(deleteMessageRequest);
        }

        private void TurnOffMonitor(Tracer Tracer)
        {
            string MonitorFinalMessage = "";
            MonitorFinalMessage = string.Format("Monitor succesfully ended at " + DateTime.Now.ToString() + ", elapsed time passed");

            if (this.RunETLThirdPartyMetrics(Tracer) && this.RunETLErrors(Tracer))
            {
                MonitorFinalMessage = string.Format("Monitor succesfully ended at " + DateTime.Now.ToString() + ", elapsed time passed");

                //CloudWatch log message
                Tracer.AddLogEvent(new TracerParameter(MonitorFinalMessage, "", System.Environment.MachineName, "", "", false, -1));
            }
            else
            {
                MonitorFinalMessage = string.Format("Monitor ended with a problem in ETLs at " + DateTime.Now.ToString() + ", elapsed time passed");

                //CloudWatch log message
                if (LogLevel == 1 || LogLevel == 2)
                {
                    Tracer.AddLogEvent(new TracerParameter(MonitorFinalMessage, "", System.Environment.MachineName, "", "", true, -1));
                }
            }

            Log.LogMessage(MonitorFinalMessage, ConfigurationManager.AppSettings["LogFileName"]);
            
            Console.WriteLine(MonitorFinalMessage);

            if (LogLevel == 1 || LogLevel == 2)
            {
                Tracer.Dispose();
            }

            Environment.Exit(0);
        }

        private Boolean RunETLThirdPartyMetrics(Tracer Tracer)
        {
            //CloudWatch log message
            Tracer.AddLogEvent(new TracerParameter("Starting ETL of data files", "", System.Environment.MachineName, "", "", false, -1));

            //Starts running the ETL process
            string odbcConnectionString = string.Concat("Driver={PostgreSQL Unicode}; Server=", endpointAddress, "; Database=", mainDbName, "; UID=", masterUsername, "; PWD=", password, "; Port=", endpointPort);
            using (OdbcConnection conn = new OdbcConnection(odbcConnectionString))
            {
                try
                {
                    conn.Open();

                    //CloudWatch log message
                    Tracer.AddLogEvent(new TracerParameter("Loading data to etl.etl_tpm_temp_metric_data_bulkload", "", System.Environment.MachineName, "", "", false, -1));

                    //ENTITY: etl.etl_tpm_temp_metric_data
                    OdbcCommand CommandInsertETLMetricDataBulk = new OdbcCommand("COPY etl.etl_tpm_temp_metric_data_bulkload (etl_date, etl_directory_id, etl_directory_name, etl_metric_name, etl_metric_value, etl_profile_address_1, etl_profile_address_2, etl_profile_business_type, etl_profile_city, etl_profile_id, etl_profile_latitude, etl_profile_longitude, etl_profile_name, etl_profile_phone, etl_profile_state, etl_profile_website, etl_profile_zip_code, etl_third_party_name, etl_third_party_profile_id, etl_third_party_url) FROM 's3://velocity-thirdpartymetrics/" + ConfigurationManager.AppSettings["S3Environment"] + "/data/"+ DateTime.Now.ToString("yyyy") + "/" + DateTime.Now.ToString("MM") + "/" + DateTime.Now.ToString("dd") + "/" + "' WITH CREDENTIALS 'aws_access_key_id=AKIAIPSFT4GVHCUNH5XQ;aws_secret_access_key=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' REGION 'us-west-2' IGNOREHEADER 1 CSV DELIMITER ',';", conn);
                    CommandInsertETLMetricDataBulk.ExecuteNonQuery();

                    //CloudWatch log message
                    Tracer.AddLogEvent(new TracerParameter("Loading data to etl.etl_tpm_temp_metric_data", "", System.Environment.MachineName, "", "", false, -1));

                    //ENTITY: etl.etl_tpm_temp_metric_data
                    OdbcCommand CommandInsertETLMetricData = new OdbcCommand("INSERT INTO etl.etl_tpm_temp_metric_data SELECT CAST(etl_date AS DATETIME), CAST(etl_directory_id AS INT), etl_directory_name, LEFT(etl_metric_name, 256), etl_metric_value, etl_profile_address_1, etl_profile_address_2, etl_profile_business_type, etl_profile_city, etl_profile_id, etl_profile_latitude, etl_profile_longitude, etl_profile_name, etl_profile_phone, etl_profile_state, etl_profile_website, etl_profile_zip_code, etl_third_party_name, etl_third_party_profile_id, etl_third_party_url FROM etl.etl_tpm_temp_metric_data_bulkload WHERE etl_metric_value <> '' AND etl_metric_value IS NOT NULL AND etl_directory_id <> '' AND etl_directory_id IS NOT NULL AND etl_date != 'Date'; ", conn);
                    CommandInsertETLMetricData.ExecuteNonQuery();

                    //CloudWatch log message
                    Tracer.AddLogEvent(new TracerParameter("Loading data to tpm.tpt_third_party", "", System.Environment.MachineName, "", "", false, -1));

                    //ENTITY: tpm.tpt_third_party
                    OdbcCommand CommandInsertThirdParty = new OdbcCommand("INSERT INTO tpm.tpt_third_party SELECT REPLACE(E.etl_third_party_name, ' ', ''), E.etl_third_party_name FROM etl.etl_tpm_temp_metric_data E WHERE NOT EXISTS(SELECT 1 FROM tpm.tpt_third_party D WHERE D.tpt_name = E.etl_third_party_name) AND E.etl_profile_id <> '' AND E.etl_profile_id IS NOT NULL AND E.etl_third_party_name <> '' AND E.etl_third_party_name IS NOT NULL GROUP BY E.etl_third_party_name; ", conn);
                    CommandInsertThirdParty.ExecuteNonQuery();

                    //CloudWatch log message
                    Tracer.AddLogEvent(new TracerParameter("Loading data to tpm.mdf_metric_definition", "", System.Environment.MachineName, "", "", false, -1));

                    //ENTITY: tpm.mdf_metric_definition
                    OdbcCommand CommandInsertMetricDefinition = new OdbcCommand("INSERT INTO tpm.mdf_metric_definition SELECT DISTINCT REPLACE(E.etl_third_party_name, ' ', '') + '_' + REPLACE(E.etl_metric_name, ' ', ''), (SELECT T.tpt_id FROM tpm.tpt_third_party T WHERE T.tpt_name = E.etl_third_party_name), E.etl_metric_name, NULL description, NULL metric_type, NULL aggregation, NULL metric_group, E.etl_metric_name friendly_name FROM etl.etl_tpm_temp_metric_data E WHERE NOT EXISTS(SELECT 1 FROM tpm.mdf_metric_definition WHERE mdf_id = REPLACE(E.etl_third_party_name, ' ', '') + '_' + REPLACE(E.etl_metric_name, ' ', '')) AND E.etl_profile_id <> '' AND E.etl_profile_id IS NOT NULL AND E.etl_third_party_name <> '' AND E.etl_third_party_name IS NOT NULL; ", conn);
                    CommandInsertMetricDefinition.ExecuteNonQuery();

                    if (ConfigurationManager.AppSettings["Delete5LastDaysGMB"].ToString() == "1")
                    {
                        //Delete last 5 days retrieved from GMB
                        OdbcCommand CommandDeleteLast5DaysGMB = new OdbcCommand("DELETE FROM tpm.mtv_metric_value WHERE mtv_tim_date >= (SELECT DATEADD(day, -5, mtv_tim_date) calculated_date FROM tpm.mtv_metric_value WHERE mtv_tpt_id = 'GoogleMyBusiness' ORDER BY mtv_tim_date DESC LIMIT 1) AND mtv_tpt_id = 'GoogleMyBusiness'", conn);
                        CommandDeleteLast5DaysGMB.ExecuteNonQuery();

                        //Delete last 5 days retrieved from GMB - Dynamic Metrics
                        OdbcCommand CommandDeleteLast5GMBDynamics = new OdbcCommand("DELETE FROM tpm.mtv_metric_value WHERE mtv_tim_date >= (SELECT DATEADD(day, -5, mtv_tim_date) calculated_date FROM tpm.mtv_metric_value WHERE mtv_tpt_id = 'GoogleMyBusiness' ORDER BY mtv_tim_date DESC LIMIT 1) AND mtv_mdf_id IN('UserDefined_GMB_TOTAL_VIEWS', 'UserDefined_GMB_TOTAL_ACTIONS', 'UserDefined_GMB_TOTAL_SEARCHES')", conn);
                        CommandDeleteLast5GMBDynamics.ExecuteNonQuery();
                    }

                    //CloudWatch log message
                    Tracer.AddLogEvent(new TracerParameter("Loading data to tpm.mtv_metric_value", "", System.Environment.MachineName, "", "", false, -1));

                    //ENTITY: tpm.mtv_metric_value
                    OdbcCommand CommandInsertMetricValue = new OdbcCommand("INSERT INTO tpm.mtv_metric_value SELECT CONCAT( md5( CONCAT( CONCAT( E.etl_profile_id, TO_CHAR(E.etl_date, 'DD/MM/YYYY') ), CONCAT( E.etl_metric_name, E.etl_third_party_name ) ) ), TO_CHAR(E.etl_date, 'DD/MM/YYYY') ), E.etl_profile_id, E.etl_date, ( SELECT tpt_id FROM tpm.tpt_third_party WHERE tpt_name = E.etl_third_party_name ), ( SELECT mdf_id FROM tpm.mdf_metric_definition WHERE mdf_tpt_id = REPLACE(E.etl_third_party_name, ' ', '') AND E.etl_metric_name = mdf_metric_name ),     E.etl_metric_name, E.etl_metric_value FROM etl.etl_tpm_temp_metric_data E WHERE NOT EXISTS( SELECT 1 FROM tpm.mtv_metric_value D WHERE D.mtv_id = CONCAT( md5( CONCAT( CONCAT( E.etl_profile_id, TO_CHAR(E.etl_date, 'DD/MM/YYYY') ), CONCAT( E.etl_metric_name, E.etl_third_party_name ) ) ), TO_CHAR(E.etl_date, 'DD/MM/YYYY') )) AND E.etl_profile_id <> '' AND E.etl_profile_id IS NOT NULL AND E.etl_third_party_name <> '' AND E.etl_third_party_name IS NOT NULL; ", conn);
                    CommandInsertMetricValue.ExecuteNonQuery();

                    //CloudWatch log message
                    Tracer.AddLogEvent(new TracerParameter("Consolidating directories name", "", System.Environment.MachineName, "", "", false, -1));

                    //Consolidates directories names
                    OdbcCommand CommandConsolidatesDirectoriesNames = new OdbcCommand("UPDATE pro.pro_profile SET pro_directory_name = 'Fidelity Bank' WHERE pro_directory_id = 114;UPDATE pro.pro_profile SET pro_directory_name = 'Red Wing Stores' WHERE pro_directory_id = 127;UPDATE pro.pro_profile SET pro_directory_name = 'Dentures & Dental Services' WHERE pro_directory_id = 176;UPDATE pro.pro_profile SET pro_directory_name = 'Cottage Health Physicians' WHERE pro_directory_id = 225;UPDATE pro.pro_profile SET pro_directory_name = 'Advocate Locations' WHERE pro_directory_id = 226;UPDATE pro.pro_profile SET pro_directory_name = 'BellinHealthPhys' WHERE pro_directory_id = 244;UPDATE pro.pro_profile SET pro_directory_name = 'AMITA Health Physicians' WHERE pro_directory_id = 290;UPDATE pro.pro_profile SET pro_directory_name = 'Things Remembered' WHERE pro_directory_id = 302; ", conn);
                    CommandConsolidatesDirectoriesNames.ExecuteNonQuery();

                    //GENERATES DYNAMIC METRICS
                    //CloudWatch log message
                    Tracer.AddLogEvent(new TracerParameter("Generating dynamic metric GMB_TOTAL_VIEWS", "", System.Environment.MachineName, "", "", false, -1));

                    OdbcCommand CommandGMB_TOTAL_VIEWS = new OdbcCommand("INSERT INTO tpm.mtv_metric_value SELECT CONCAT(md5(CONCAT(CONCAT(E.etl_profile_id, TO_CHAR(E.etl_date, 'DD/MM/YYYY')), CONCAT('GMB_TOTAL_VIEWS', 'UserDefined'))), TO_CHAR(E.etl_date, 'DD/MM/YYYY')) id, E.etl_profile_id pro_id, E.etl_date date, 'UserDefined' tpt_id, 'UserDefined_GMB_TOTAL_VIEWS' mdf_id, 'GMB_TOTAL_VIEWS' metric_name, SUM(E.etl_metric_value) value FROM etl.etl_tpm_temp_metric_data E WHERE E.etl_metric_name IN('VIEWS_SEARCH', 'VIEWS_MAPS') AND E.etl_third_party_name = 'Google My Business' GROUP BY E.etl_date, E.etl_profile_id;", conn);
                    CommandGMB_TOTAL_VIEWS.ExecuteNonQuery();

                    //CloudWatch log message
                    Tracer.AddLogEvent(new TracerParameter("Generating dynamic metric GMB_TOTAL_ACTIONS", "", System.Environment.MachineName, "", "", false, -1));

                    OdbcCommand CommandGMB_TOTAL_ACTIONS = new OdbcCommand("INSERT INTO tpm.mtv_metric_value SELECT CONCAT(md5(CONCAT(CONCAT(E.etl_profile_id, TO_CHAR(E.etl_date, 'DD/MM/YYYY')), CONCAT('GMB_TOTAL_ACTIONS', 'UserDefined'))), TO_CHAR(E.etl_date, 'DD/MM/YYYY')) id, E.etl_profile_id pro_id, E.etl_date date, 'UserDefined' tpt_id, 'UserDefined_GMB_TOTAL_ACTIONS' mdf_id, 'GMB_TOTAL_ACTIONS' metric_name, SUM(E.etl_metric_value) value FROM etl.etl_tpm_temp_metric_data E WHERE E.etl_metric_name IN('ACTIONS_WEBSITE', 'ACTIONS_DRIVING_DIRECTIONS', 'ACTIONS_PHONE') AND E.etl_third_party_name = 'Google My Business' GROUP BY E.etl_date, E.etl_profile_id;", conn);
                    CommandGMB_TOTAL_ACTIONS.ExecuteNonQuery();

                    //CloudWatch log message
                    Tracer.AddLogEvent(new TracerParameter("Generating dynamic metric GMB_TOTAL_SEARCHES", "", System.Environment.MachineName, "", "", false, -1));

                    OdbcCommand CommandGMB_TOTAL_SEARCHES = new OdbcCommand("INSERT INTO tpm.mtv_metric_value SELECT CONCAT(md5(CONCAT(CONCAT(E.etl_profile_id, TO_CHAR(E.etl_date, 'DD/MM/YYYY')), CONCAT('GMB_TOTAL_SEARCHES', 'UserDefined'))), TO_CHAR(E.etl_date, 'DD/MM/YYYY')) id, E.etl_profile_id pro_id, E.etl_date date, 'UserDefined' tpt_id, 'UserDefined_GMB_TOTAL_SEARCHES' mdf_id, 'GMB_TOTAL_SEARCHES' metric_name, SUM(E.etl_metric_value) value FROM etl.etl_tpm_temp_metric_data E WHERE E.etl_metric_name IN('QUERIES_INDIRECT', 'QUERIES_DIRECT') AND E.etl_third_party_name = 'Google My Business' GROUP BY E.etl_date, E.etl_profile_id;", conn);
                    CommandGMB_TOTAL_SEARCHES.ExecuteNonQuery();

                    //CloudWatch log message
                    Tracer.AddLogEvent(new TracerParameter("Generating dynamic metric GA_TOTAL_SESSIONS", "", System.Environment.MachineName, "", "", false, -1));

                    OdbcCommand CommandGA_TOTAL_SESSIONS = new OdbcCommand("INSERT INTO tpm.mtv_metric_value SELECT CONCAT(md5(CONCAT(CONCAT(E.etl_profile_id, TO_CHAR(E.etl_date, 'DD/MM/YYYY')), CONCAT('GA_TOTAL_SESSIONS', 'UserDefined'))), TO_CHAR(E.etl_date, 'DD/MM/YYYY')) id, E.etl_profile_id pro_id, E.etl_date date, 'UserDefined' tpt_id, 'UserDefined_GA_TOTAL_SESSIONS' mdf_id, 'GA_TOTAL_SESSIONS' metric_name, SUM(E.etl_metric_value) value FROM etl.etl_tpm_temp_metric_data E WHERE E.etl_metric_name LIKE '%-Session' AND E.etl_third_party_name = 'Google Analytics' GROUP BY E.etl_date, E.etl_profile_id;", conn);
                    CommandGA_TOTAL_SESSIONS.ExecuteNonQuery();

                    //CloudWatch log message
                    Tracer.AddLogEvent(new TracerParameter("Finished ETL of data files", "", System.Environment.MachineName, "", "", false, -1));

                    return true;
                }
                catch (AmazonRedshiftException e)
                {
                    //TODO: Append to error string log
                    if (e.ErrorCode != null &&
                        (e.ErrorCode.Equals("InvalidAccessKeyId")
                        ||
                        e.ErrorCode.Equals("InvalidSecurity")))
                    {
                        Log.LogMessage(string.Format("Check the provided AWS Credentials. {0}", e), ConfigurationManager.AppSettings["LogFileErrorName"]);
                    }
                    else
                    {
                        Log.LogMessage(string.Format("Error occurred. Message:'{0}' when running the ETL", e.Message), ConfigurationManager.AppSettings["LogFileErrorName"]);
                    }

                    return false;
                }
            }
        }

        private Boolean RunETLErrors(Tracer Tracer)
        {
            //CloudWatch log message
            Tracer.AddLogEvent(new TracerParameter("Starting ETL of errors files", "", System.Environment.MachineName, "", "", false, -1));

            //Starts running the ETL process with data existing in temporary errors table
            string odbcConnectionString = string.Concat("Driver={PostgreSQL Unicode}; Server=", endpointAddress, "; Database=", mainDbName, "; UID=", masterUsername, "; PWD=", password, "; Port=", endpointPort);
            using (OdbcConnection conn = new OdbcConnection(odbcConnectionString))
            {
                try
                {
                    conn.Open();

                    //CloudWatch log message
                    Tracer.AddLogEvent(new TracerParameter("Loading temp data to etl.temp_data_producer_error_log", "", System.Environment.MachineName, "", "", false, -1));

                    //ENTITY: etl.temp_data_producer_error_log
                    OdbcCommand CommandInsertTempError = new OdbcCommand("COPY etl.temp_data_producer_error_log FROM 's3://velocity-thirdpartymetrics/" + ConfigurationManager.AppSettings["S3Environment"] + "/error_logs/" + DateTime.Now.ToString("yyyy") + "/" + DateTime.Now.ToString("MM") + "/" + DateTime.Now.ToString("dd") + "/" + "' WITH CREDENTIALS 'aws_access_key_id=AKIAIPSFT4GVHCUNH5XQ;aws_secret_access_key=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' REGION 'us-west-2' IGNOREHEADER 0 CSV DELIMITER ',';", conn);
                    CommandInsertTempError.ExecuteNonQuery();

                    //CloudWatch log message
                    Tracer.AddLogEvent(new TracerParameter("Inserting data to tpm.del_data_producer_error_log", "", System.Environment.MachineName, "", "", false, -1));

                    //ENTITY: tpm.del_data_producer_error_log
                    OdbcCommand CommandInsertError = new OdbcCommand("INSERT INTO tpm.del_data_producer_error_log SELECT * FROM etl.temp_data_producer_error_log E WHERE NOT EXISTS(SELECT DISTINCT 1 FROM tpm.del_data_producer_error_log DEL WHERE E.etl_time = DEL.del_time AND E.etl_tpt_id = DEL.del_tpt_id AND E.etl_error_message = DEL.del_error_message AND E.etl_error_type = DEL.del_error_type)", conn);
                    CommandInsertError.ExecuteNonQuery();

                    return true;
                }
                catch (AmazonRedshiftException e)
                {
                    //TODO: Append to error string log
                    if (e.ErrorCode != null &&
                        (e.ErrorCode.Equals("InvalidAccessKeyId")
                        ||
                        e.ErrorCode.Equals("InvalidSecurity")))
                    {
                        Log.LogMessage(string.Format("Check the provided AWS Credentials. {0}", e), ConfigurationManager.AppSettings["LogFileErrorName"]);
                    }
                    else
                    {
                        Log.LogMessage(string.Format("Error occurred. Message:'{0}' when running the ETL for errors", e.Message), ConfigurationManager.AppSettings["LogFileErrorName"]);
                    }

                    return false;
                }
            }

            //CloudWatch log message
            Tracer.AddLogEvent(new TracerParameter("Finished ETL of errors files", "", System.Environment.MachineName, "", "", false, -1));
        }
    }
}

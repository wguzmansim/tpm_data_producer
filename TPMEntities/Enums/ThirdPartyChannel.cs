﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TPMEntities.Enums
{
    public enum ThirdPartyChannel
    {
        GoogleMyBusiness = 6,
        GoogleAnalytics = 20
    }
}

﻿namespace TPMEntities
{
    public class TPMQueueMessage
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public string DirectoryID { get; set; }
        public string DirectoryName { get; set; }
        public string AccountID { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string BusinessType { get; set; }
        public string City { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string Phone { get; set; }
        public string State { get; set; }
        public string Website { get; set; }
        public string ZipCode { get; set; }
        public string StoreID { get; set; }
        //public string ThirdPartyChannel { get; set; }
        public Enums.ThirdPartyChannel ThirdPartyChannel { get; set; }

        public string ThirdPartyChannelName { get; set; }
        public string ThirdPartyProfileID { get; set; }
        public string ThirdPartyProfileURL { get; set; }
        public string StartDateMetrics { get; set; }
        public string EndDateMetrics { get; set; }
        public string ProcessStartDate { get; set; }
    }
}
﻿using System;
using System.Configuration;
using System.IO;

namespace TPMLog
{
    public class Log
    {
        public string AccumulatedLogMessage { get; set; }

        public static void LogMessage(string Message, string FileName)
        {
            try
            {
                var FilePath = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + "\\" + FileName;
                if (!Directory.Exists(Path.GetDirectoryName(FilePath)))
                {
                    Directory.CreateDirectory(Path.GetDirectoryName(FilePath));
                }
                File.AppendAllText(FilePath,  String.Format("{0}", Message) + "\r\n");
            }
            catch
            {
                //TODO: Append to error string log
            }
        }

        public static void LogMessage(string Message, string FileName, DateTime ProcessStartDate)
        {
            var NewFileName = Path.GetFileNameWithoutExtension(FileName) + "_" + ProcessStartDate.ToString("yyyy-MM-dd_HH_mm_ss") + Path.GetExtension(FileName);
            LogMessage(Message, NewFileName);
        }

        //Collects all error logs, saves the CSV in the S3 bucket, and loads this file in Redshift ETL temporary table
        public void ReportError(string LocalFile, string DirectoryID, string ProfileID, string ComponentType)
        {
            //Saves accumulated error string in unique local file
            string LocalFolder = ConfigurationManager.AppSettings["ErrorsCSV"];

            if (this.AccumulatedLogMessage != "" && !String.IsNullOrEmpty(this.AccumulatedLogMessage))
            {
                string FilenameString = string.Format("TPMWorker_DirectoryID_{1}_Profile_{2}_DateTime_{3}.csv", ComponentType, DirectoryID, ProfileID, DateTime.Now.ToString("yyyyMMdd"));
                string LocalUniqueFilename = LocalFolder + "\\\\" + FilenameString;
                File.WriteAllText(LocalUniqueFilename, this.AccumulatedLogMessage);

                //Saves unique local file in S3 bucket
                SIMAmazon AmazonS3 = new SIMAmazon();
                string DestinationFilename = FilenameString;

                string BucketName = ConfigurationManager.AppSettings["S3ErrorsBucketPath"];
                AmazonS3.SaveToS3(BucketName, LocalUniqueFilename, DateTime.Now.ToString("yyyy") + "/" + DateTime.Now.ToString("MM") + "/" + DateTime.Now.ToString("dd") + "/" + DestinationFilename, DateTime.Now);
            }
        }
    }
}
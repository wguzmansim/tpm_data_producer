﻿using System;
using System.Configuration;
using System.Data.Odbc;

using Amazon.Redshift;
using Amazon.S3;
using Amazon.S3.Model;

using TPMLog;

namespace TPMLog
{
    public class SIMAmazon
    {
        static IAmazonS3 client = new AmazonS3Client(Amazon.RegionEndpoint.USEast1);

        public Boolean SaveToS3(string BucketName, string SourceFilePath, string DestinationFilename, DateTime ProcessStartDate)
        {
            try
            {
                PutObjectRequest putRequest2 = new PutObjectRequest
                {
                    BucketName = BucketName,
                    Key = DestinationFilename,
                    FilePath = SourceFilePath,
                    ContentType = "text/plain"
                };
                putRequest2.Metadata.Add("x-amz-meta-title", "someTitle");

                PutObjectResponse response2 = client.PutObject(putRequest2);

                return true;
            }
            catch (AmazonS3Exception amazonS3Exception)
            {
                if (amazonS3Exception.ErrorCode != null &&
                    (amazonS3Exception.ErrorCode.Equals("InvalidAccessKeyId")
                    ||
                    amazonS3Exception.ErrorCode.Equals("InvalidSecurity")))
                {
                    Log.LogMessage(string.Format("Check the provided AWS Credentials. {0}", amazonS3Exception), ConfigurationManager.AppSettings["LogFileErrorName"]);
                }
                else
                {
                    //TODO: Append to error string log
                    Log.LogMessage(string.Format("Error occurred. Message:'{0}' when writing an object", amazonS3Exception.Message), ConfigurationManager.AppSettings["LogFileErrorName"]);
                }

                return false;
            }
        }

        public void LoadErrorLogCSVToRedshift(string FileToLoad)
        {
            String mainDbName = ConfigurationManager.AppSettings["RedshiftDBName"];
            String endpointAddress = ConfigurationManager.AppSettings["RedshiftHost"];
            String endpointPort = ConfigurationManager.AppSettings["RedshiftPort"];
            string masterUsername = ConfigurationManager.AppSettings["RedshiftUsername"];
            string password = ConfigurationManager.AppSettings["RedshiftPassword"];

            Log.LogMessage(string.Format("Before creating connection string"), ConfigurationManager.AppSettings["LogFileErrorName"]);

            string odbcConnectionString = string.Concat("Driver={PostgreSQL Unicode}; Server=", endpointAddress, "; Database=", mainDbName, "; UID=", masterUsername, "; PWD=", password, "; Port=", endpointPort);
            using (OdbcConnection conn = new OdbcConnection(odbcConnectionString))
            {
                try
                {
                    Log.LogMessage(string.Format("Trying to connect"), ConfigurationManager.AppSettings["LogFileErrorName"]);

                    conn.Open();
                    //Load the CSV file into the temporary table
                    OdbcCommand CommandCopy = new OdbcCommand("COPY etl.temp_data_producer_error_log FROM '" + FileToLoad + "' WITH CREDENTIALS 'aws_access_key_id=AKIAIPSFT4GVHCUNH5XQ;aws_secret_access_key=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' REGION 'us-west-2' IGNOREBLANKLINES IGNOREHEADER 0 CSV DELIMITER ',';", conn);

                    Log.LogMessage(string.Format("Before copy command, COPY etl.temp_data_producer_error_log FROM '" + FileToLoad + "' WITH CREDENTIALS 'aws_access_key_id=AKIAIPSFT4GVHCUNH5XQ;aws_secret_access_key=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' REGION 'us-west-2' IGNOREBLANKLINES IGNOREHEADER 0 CSV DELIMITER ',';"), ConfigurationManager.AppSettings["LogFileErrorName"]);

                    CommandCopy.ExecuteNonQuery();
                }
                catch (AmazonRedshiftException e)
                {
                    //TODO: Append to error string log
                    if (e.ErrorCode != null &&
                        (e.ErrorCode.Equals("InvalidAccessKeyId")
                        ||
                        e.ErrorCode.Equals("InvalidSecurity")))
                    {
                        Log.LogMessage(string.Format("Check the provided AWS Credentials. {0}", e), ConfigurationManager.AppSettings["LogFileErrorName"]);
                    }
                    else
                    {
                        Log.LogMessage(string.Format("Error occurred. Message:'{0}' when loading error log CSV file", e.Message), ConfigurationManager.AppSettings["LogFileErrorName"]);
                    }
                }
            }
        }
    }
}
﻿using System.Collections.Generic;

namespace TPMStarter
{
    class DirectoryIDsAPIData
    {
        public Status Status { get; set; }
        public List<Directory> Items { get; set; }
    }
    public class Directory
    {
        public string DirectoryID { get; set; }
        public string AccountID { get; set; }
    }
    public class Status
    {
        public bool Success { get; set; }
        public object Error { get; set; }
    }
}

﻿namespace TPMStarter
{
    public class DirectoryIDsAPIToken
    {
        public bool Success { get; set; }
        public string Token { get; set; }
        public object Error { get; set; }
    }
}

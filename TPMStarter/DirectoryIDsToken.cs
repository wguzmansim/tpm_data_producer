﻿namespace TPMStarter
{
    public class DirectoryIDsToken
    {
        public bool Success { get; set; }
        public string Token { get; set; }
        public object Error { get; set; }
    }
}

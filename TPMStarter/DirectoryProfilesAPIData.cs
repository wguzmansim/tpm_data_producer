﻿using System.Collections.Generic;

namespace TPMStarter
{
    public class DirectoryProfilesAPIData
    {
        public StatusAPIData Status { get; set; }
        public List<VelocityProfile> Items { get; set; }
    }
    public class VelocityProfile
    {
        public string pcn_ID { get; set; }
        public string pcn_pro_ID { get; set; }
        public string pcn_chn_ID { get; set; }
        public string chn_Name { get; set; }
        public string pcn_ChannelExternalId { get; set; }
        public string pcn_pcs_ID { get; set; }
        public string pcs_Name { get; set; }
        public string pcs_Description { get; set; }
        public string pcn_IsActive { get; set; }
        public string pcn_CreateTimestamp { get; set; }
        public string DirectoryID { get; set; }
        public string DirectoryName { get; set; }
        public string ProfileAddress1 { get; set; }
        public string ProfileAddress2 { get; set; }
        public string ProfileBusinessType { get; set; }
        public string ProfileCity { get; set; }
        public string ProfileID { get; set; }
        public string ProfileLatitude { get; set; }
        public string ProfileLongitude { get; set; }
        public string ProfileName { get; set; }
        public string ProfilePhone { get; set; }
        public string ProfileState { get; set; }
        public string ProfileWebsite { get; set; }
        public string ProfileZipCode { get; set; }
        public string ThirdPartyChannelName { get; set; }
        public string ThirdPartyProfileID { get; set; }
        public string ThirdPartyURL { get; set; }
        public string ProfileTollFreeNumber { get; set; }
        public string ProfileImageUrl { get; set; }
    }
    public class StatusAPIData
    {
        public bool Success { get; set; }
        public object Error { get; set; }
    }
}

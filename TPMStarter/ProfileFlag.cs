﻿namespace TPMStarter
{
    class ProfileFlag
    {
        public string ID { get; set; }
        public string DirectoryID { get; set; }
        public bool Completed { get; set; }
    }
}

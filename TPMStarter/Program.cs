﻿extern alias AWSSQS;

using AWSSQS::Amazon.SQS;
using AWSSQS::Amazon.SQS.Model;

using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Net;
using Newtonsoft.Json;
using TPMEntities;
using TPMLog;
using VelocityTracer;
using System.Globalization;

namespace TPMStarter
{
    class Program
    {
        static string DirectoryIDsAPIURL = ConfigurationManager.AppSettings["DirectoryIDsAPIURL"];
        static string VelocityTokenAPIURL = ConfigurationManager.AppSettings["VelocityTokenAPIURL"];
        static string VelocityTokenAPIKey = ConfigurationManager.AppSettings["VelocityTokenAPIKey"];
        public static Tracer Tracer = new Tracer("TPMStarter");
        static int LogLevel = int.Parse(ConfigurationManager.AppSettings["LogLevel"]);

        static void Main(string[] args)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;

            string LogMessage = String.Format("TPMStarter initializes at {0}", DateTime.Now.ToString());
            Console.WriteLine(LogMessage);

            //CloudWatch log message
            if (LogLevel == 2)
            {
                Tracer.AddLogEvent(new TracerParameter(LogMessage, "", System.Environment.MachineName, "", "", false, -1));
            }

            string TPMDataProducerQueue = ConfigurationManager.AppSettings["TPMDataProducerQueue"];

            //Checks if passed the ThirdPartyMetrics provider parameter
            Boolean StarterError = false;
            string ErrorMessage = "";
            string ParameterName = "";
            string ThirdPartyChannel = "";
            if (args.Length == 0)
            {
                ErrorMessage = "No ThirdPartyMetrics provider for Starter";
                StarterError = true;

                //CloudWatch error log message
                if (LogLevel == 1 || LogLevel == 2)
                {
                    Tracer.AddLogEvent(new TracerParameter(ErrorMessage, "", System.Environment.MachineName, "", "", true, -1));
                }
            }
            else
            {
                string[] StarterParameters = args[0].Split('=');
                ParameterName = StarterParameters[0];
                ThirdPartyChannel = StarterParameters[1];

                //Checks if Starter received a valid parameter ("tpc")
                if (ParameterName != "tpc")
                {
                    ErrorMessage = "No ThirdPartyMetrics provider for Starter";
                    Console.WriteLine(ErrorMessage);
                    StarterError = true;

                    //CloudWatch error log message
                    if (LogLevel == 1 || LogLevel == 2)
                    {
                        Tracer.AddLogEvent(new TracerParameter(ErrorMessage, "", System.Environment.MachineName, "", "", true, -1));
                    }
                }
                else
                {
                    TPMQueueMessage QueueMessage = new TPMQueueMessage();
                    var sqs = new AmazonSQSClient();
                    switch (ThirdPartyChannel)
                    {
                        case "gmb":
                            //Forces GMBWorkers execution
                            Starter GMBStarter = new Starter();
                            GMBStarter.RunWorkers("gmb", Tracer);
                            break;
                        case "ga":
                            sqs = new AmazonSQSClient();

                            //Loops through DirectoryIDs API data
                            DirectoryIDsAPIData DirectoryList = GetDirectoryIDsData();
                            int ProfilesStoredProcedure = DirectoryList.Items.Count;

                            //Sends metric for profiles retrieved for GA
                            MetricParameter metric = new MetricParameter("GAProfilesRetrieved", ProfilesStoredProcedure, MetricUnit.Count,
                                new List<Dimension>() {
                                new Dimension("Name","TotalProfiles"),
                                new Dimension("Value",ProfilesStoredProcedure.ToString())
                            });
                            Tracer.AddMetric(metric);
                            Tracer.SendMetric(ConfigurationManager.AppSettings["MetricNameSpace"]);

                            LogMessage = string.Format("{0} profile(s) retrieved from stored procedure for GA.", ProfilesStoredProcedure);

                            //CloudWatch log message
                            if (LogLevel == 2)
                            {
                                Tracer.AddLogEvent(new TracerParameter(LogMessage, "", System.Environment.MachineName, "", "", false, -1));
                            }

                            foreach (var Directory in DirectoryList.Items)
                            {
                                if (Directory.DirectoryID != "")
                                {
                                    string QueueMessageBody = "{\"DirectoryID\": \"" + Directory.DirectoryID + "\",\"AccountID\": \"" + Directory.AccountID + "\",\"ThirdPartyChannel\": \"GoogleAnalytics\",\"StartDateMetrics\": \"" + ConfigurationManager.AppSettings["StartDateMetrics"] + "\",\"EndDateMetrics\": \"" + ConfigurationManager.AppSettings["EndDateMetrics"] + "\"}";
                                    var sendMessageRequest = new SendMessageRequest
                                    {
                                        QueueUrl = TPMDataProducerQueue,
                                        MessageBody = QueueMessageBody
                                    };
                                    sqs.SendMessage(sendMessageRequest);
                                }
                            }
                            break;
                        default:
                            ErrorMessage = "Wrong ThirdPartyChannel provided for Starter. It must be gmb or ga";
                            Console.WriteLine(ErrorMessage);
                            StarterError = true;

                            //CloudWatch error log message
                            if (LogLevel == 1 || LogLevel == 2)
                            {
                                Tracer.AddLogEvent(new TracerParameter(ErrorMessage, "", System.Environment.MachineName, "", "", true, -1));
                            }

                            break;
                    }
                }
            }

            if (StarterError == true)
            {
                //TODO: Append to error string log
                ErrorMessage = "TPMStarter failed at" + DateTime.Now.ToString() + ", Error = " + ErrorMessage;
                Console.WriteLine(ErrorMessage);

                //CloudWatch error log message
                if (LogLevel == 1 || LogLevel == 2)
                {
                    Tracer.AddLogEvent(new TracerParameter(ErrorMessage, "", System.Environment.MachineName, "", "", true, -1));
                }
            }
            else
            {
                ErrorMessage = "TPMStarter ended at " + DateTime.Now.ToString() + ", for ThirdPartyChannel = " + ThirdPartyChannel;
                Console.WriteLine(ErrorMessage);

                //CloudWatch log message
                if (LogLevel == 2)
                {
                    Tracer.AddLogEvent(new TracerParameter(ErrorMessage, "", System.Environment.MachineName, "", "", false, -1));
                }
            }

            if (LogLevel == 1 || LogLevel == 2)
            {
                Tracer.Dispose();
            }

            return;
        }

        static DirectoryIDsAPIData GetDirectoryIDsData()
        {
            DirectoryIDsAPIData DirectoryIDsAPIDataResult = new DirectoryIDsAPIData();

            try
            {
                //Tries to get a token for API call
                string DirectoryIDDataAPITokenCalculated = GetDirectoryIDsToken();
                if (DirectoryIDDataAPITokenCalculated == "")
                {
                    return new DirectoryIDsAPIData();
                }

                var httpWebRequest = (HttpWebRequest)WebRequest.Create(DirectoryIDsAPIURL);
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "GET";
                httpWebRequest.Headers["Token"] = DirectoryIDDataAPITokenCalculated;
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                var result = "";
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    result = streamReader.ReadToEnd();
                }
                DirectoryIDsAPIDataResult = JsonConvert.DeserializeObject<DirectoryIDsAPIData>(result);

                string LogMessage = string.Format("Finished getting directory ID data");
                Log.LogMessage(LogMessage, ConfigurationManager.AppSettings["LogFileName"]);

                //CloudWatch error log message
                if (LogLevel == 2)
                {
                    Tracer.AddLogEvent(new TracerParameter(LogMessage, "", System.Environment.MachineName, "", "", false, -1));
                }
            }
            catch (Exception e)
            {
                string ErrorMessage = string.Format("Error getting directory ID data, error = {0}", e.Message);
                Log.LogMessage(ErrorMessage, ConfigurationManager.AppSettings["LogFileErrorName"]);

                //CloudWatch error log message
                if (LogLevel == 1 || LogLevel == 2)
                {
                    Tracer.AddLogEvent(new TracerParameter(ErrorMessage, "", System.Environment.MachineName, "", "", true, -1));
                }

                if (LogLevel == 1 || LogLevel == 2)
                {
                    Tracer.Dispose();
                }

                Environment.Exit(3);
            }

            return DirectoryIDsAPIDataResult;
        }

        static string GetDirectoryIDsToken()
        {
            try
            {
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(VelocityTokenAPIURL);
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "POST";

                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    string json = "{\"ApiKey\":\"" + VelocityTokenAPIKey + "\"}";

                    streamWriter.Write(json);
                    streamWriter.Flush();
                    streamWriter.Close();
                }

                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                var result = "";
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    result = streamReader.ReadToEnd();
                }
                var ProfileDataAPITokenResponse = JsonConvert.DeserializeObject<DirectoryIDsAPIToken>(result);
                if (ProfileDataAPITokenResponse.Success)
                {
                    return ProfileDataAPITokenResponse.Token;
                }
                else
                {
                    return String.Empty;
                }
            }
            catch (Exception e)
            {
                string ErrorMessage = string.Format("Error getting token for DirectoryIDsData, error = {0}", e.Message);
                Log.LogMessage(ErrorMessage, ConfigurationManager.AppSettings["LogFileErrorName"]);

                if (LogLevel == 1 || LogLevel == 2)
                {
                    //CloudWatch error log message
                    Tracer.AddLogEvent(new TracerParameter(ErrorMessage, "", System.Environment.MachineName, "", "", true, -1));
                }

                return String.Empty;
            }
        }
    }
}
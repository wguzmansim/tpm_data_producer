﻿extern alias AWSSQS;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Data;
using Newtonsoft.Json;
using TPMEntities;
using System.Net;

using TPMLog;
using AWSSQS::Amazon.SQS;
using AWSSQS::Amazon.SQS.Model;

using VelocityTracer;
using System.Globalization;
using System.Reflection;

namespace TPMStarter
{
    
    public class Starter
    {
        private DateTime ProcessStartDate;
        DataTable ProfileTable = new DataTable();
        static string DirectoryProfilesAPIURL = ConfigurationManager.AppSettings["DirectoryProfilesAPIURL"];
        static string VelocityTokenAPIURL = ConfigurationManager.AppSettings["VelocityTokenAPIURL"];
        static string VelocityTokenAPIKey = ConfigurationManager.AppSettings["VelocityTokenAPIKey"];
        static string TPMDataProducerQueue = ConfigurationManager.AppSettings["TPMDataProducerQueue"];
        static int LogLevel = int.Parse(ConfigurationManager.AppSettings["LogLevel"]);
        public int Repetitions = 0;
        public int ProfilesStoredProcedure = 0;

        public Starter()
        {
            ProcessStartDate = DateTime.Now;
        }

        public void RunWorkers(string ThirdPartyChannel, Tracer Tracer)
        {
            var sqs = new AmazonSQSClient();
            this.GetProfileData(Tracer);
            ProfilesStoredProcedure = ProfileTable.Rows.Count;

            string LogMessage = string.Format("{0} profile(s) retrieved from stored procedure for GMB.", ProfilesStoredProcedure);
            Console.WriteLine(LogMessage);

            MetricParameter metric = new MetricParameter("GMBProfilesRetrieved", ProfilesStoredProcedure, MetricUnit.Count,
                new List<Dimension>() {
                new Dimension("Name","TotalProfiles"),
                new Dimension("Value",ProfilesStoredProcedure.ToString())
            });
            Tracer.AddMetric(metric);
            Tracer.SendMetric(ConfigurationManager.AppSettings["MetricNameSpace"]);

            //CloudWatch log message
            if (LogLevel == 2)
            {
                Tracer.AddLogEvent(new TracerParameter(LogMessage, "", System.Environment.MachineName, "", "", false, -1));
            }

            foreach (DataRow RowProfile in ProfileTable.Rows)
            {
               var ProfileData = GetProfile(RowProfile);

                //If we only will work with some directories
                if (ConfigurationManager.AppSettings["GMBOnlyDirectories"] != "")
                {
                    string[] stringSeparators = new string[] { "," };
                    string[] GMBOnlyDirectoriesArray = ConfigurationManager.AppSettings["GMBOnlyDirectories"].ToString().Split(stringSeparators, StringSplitOptions.None);
                    int index = Array.IndexOf(GMBOnlyDirectoriesArray, ProfileData.DirectoryID);

                    if (index < 0)
                    {
                        continue;
                    }
                }

                try
                {
                    //Adjusts start and end dates to get the day with available metrics
                    DateTime NewStartDate = DateTime.ParseExact(ConfigurationManager.AppSettings["StartDateMetrics"], "yyyy-MM-dd", CultureInfo.InvariantCulture);
                    string NewStartDateString = NewStartDate.AddDays(-3).ToString("yyyy-MM-dd");
                    string NewEndDateString = NewStartDate.AddDays(-2).ToString("yyyy-MM-dd");

                    //Register SQS for current profile
                    string QueueMessageBody = "{\"DirectoryID\":\"" + ProfileData.DirectoryID + "\",\"DirectoryName\":\"" + ProfileData.DirectoryName + "\",\"ID\":\"" + ProfileData.ID + "\",\"Name\":\"" + ProfileData.Name + "\",\"Address1\":\"" + ProfileData.Address1 + "\",\"Address2\":\"" + ProfileData.Address2 + "\",\"BusinessType\":\"" + ProfileData.BusinessType + "\",\"City\":\"" + ProfileData.City + "\",\"Latitude\":\"" + ProfileData.Latitude + "\",\"Longitude\":\"" + ProfileData.Longitude + "\",\"Phone\":\"" + ProfileData.Phone + "\",\"State\":\"" + ProfileData.State + "\",\"Website\":\"" + ProfileData.Website + "\",\"ZipCode\":\"" + ProfileData.ZipCode + "\",\"ThirdPartyChannel\":\"GoogleMyBusiness\",\"ThirdPartyChannelName\":\"" + ProfileData.ThirdPartyChannelName + "\",\"ThirdPartyProfileID\":\"" + ProfileData.ThirdPartyProfileID + "\",\"ThirdPartyURL\":\"" + ProfileData.ThirdPartyURL + "\",\"StartDateMetrics\":\"" + NewStartDateString + "\",\"EndDateMetrics\":\"" + NewEndDateString + "\",\"ProcessStartDate\":\"" + ProfileData.ProcessStartDate.ToString("yyyy-MM-dd") + "\"}";
                    var sendMessageRequest = new SendMessageRequest
                    {
                        QueueUrl = TPMDataProducerQueue,
                        MessageBody = QueueMessageBody
                    };
                    sqs.SendMessage(sendMessageRequest);
                }
                catch (Exception e)
                {
                    LogMessage = string.Format("Error creating SQS message, details = {0}", e.Message);
                    Log.LogMessage(LogMessage, ConfigurationManager.AppSettings["LogFileErrorName"]);

                    //CloudWatch error log message
                    if (LogLevel == 1 || LogLevel == 2)
                    {
                        Tracer.AddLogEvent(new TracerParameter(LogMessage, "", System.Environment.MachineName, "", "", true, -1));
                    }
                }
            }
        }

        private void GetProfileData(Tracer Tracer)
        {
            //Uses API service to get all directories information
            DirectoryProfilesAPIData DirectoriesAPIData = new DirectoryProfilesAPIData();
            while (DirectoriesAPIData.Items == null)
            {
                DirectoriesAPIData = GetDirectoryProfilesAPIData(Tracer);
            }
            

            ProfileTable = new DataTable();
            ProfileTable.Columns.Add("pcn_ID", typeof(string));
            ProfileTable.Columns.Add("pcn_pro_ID", typeof(string));
            ProfileTable.Columns.Add("pcn_chn_ID", typeof(string));
            ProfileTable.Columns.Add("chn_Name", typeof(string));
            ProfileTable.Columns.Add("pcn_ChannelExternalId", typeof(string));
            ProfileTable.Columns.Add("pcn_pcs_ID", typeof(string));
            ProfileTable.Columns.Add("pcs_Name", typeof(string));
            ProfileTable.Columns.Add("pcs_Description", typeof(string));
            ProfileTable.Columns.Add("pcn_IsActive", typeof(string));
            ProfileTable.Columns.Add("pcn_CreateTimestamp", typeof(string));
            ProfileTable.Columns.Add("DirectoryID", typeof(string));
            ProfileTable.Columns.Add("DirectoryName", typeof(string));
            ProfileTable.Columns.Add("ProfileAddress1", typeof(string));
            ProfileTable.Columns.Add("ProfileAddress2", typeof(string));
            ProfileTable.Columns.Add("ProfileBusinessType", typeof(string));
            ProfileTable.Columns.Add("ProfileCity", typeof(string));
            ProfileTable.Columns.Add("ProfileID", typeof(string));
            ProfileTable.Columns.Add("ProfileLatitude", typeof(string));
            ProfileTable.Columns.Add("ProfileLongitude", typeof(string));
            ProfileTable.Columns.Add("ProfileName", typeof(string));
            ProfileTable.Columns.Add("ProfilePhone", typeof(string));
            ProfileTable.Columns.Add("ProfileState", typeof(string));
            ProfileTable.Columns.Add("ProfileWebsite", typeof(string));
            ProfileTable.Columns.Add("ProfileZipCode", typeof(string));
            ProfileTable.Columns.Add("ThirdPartyChannelName", typeof(string));
            ProfileTable.Columns.Add("ThirdPartyProfileID", typeof(string));
            ProfileTable.Columns.Add("ThirdPartyURL", typeof(string));
            ProfileTable.Columns.Add("ProfileTollFreeNumber", typeof(string));
            ProfileTable.Columns.Add("ProfileImageUrl", typeof(string));

            foreach (VelocityProfile Profile in DirectoriesAPIData.Items)
            {
                DataRow ProfileRow = ProfileTable.NewRow();
                object[] RowArray = new object[29];
                RowArray[0] = Profile.pcn_ID;
                RowArray[1] = Profile.pcn_pro_ID;
                RowArray[2] = Profile.pcn_chn_ID;
                RowArray[3] = Profile.chn_Name;
                RowArray[4] = Profile.pcn_ChannelExternalId;
                RowArray[5] = Profile.pcn_pcs_ID;
                RowArray[6] = Profile.pcs_Name;
                RowArray[7] = Profile.pcs_Description;
                RowArray[8] = Profile.pcn_IsActive;
                RowArray[9] = Profile.pcn_CreateTimestamp;
                RowArray[10] = Profile.DirectoryID;
                RowArray[11] = Profile.DirectoryName;
                RowArray[12] = Profile.ProfileAddress1;
                RowArray[13] = Profile.ProfileAddress2;
                RowArray[14] = Profile.ProfileBusinessType;
                RowArray[15] = Profile.ProfileCity;
                RowArray[16] = Profile.ProfileID;
                RowArray[17] = Profile.ProfileLatitude;
                RowArray[18] = Profile.ProfileLongitude;
                RowArray[19] = Profile.ProfileName;
                RowArray[20] = Profile.ProfilePhone;
                RowArray[21] = Profile.ProfileState;
                RowArray[22] = Profile.ProfileWebsite;
                RowArray[23] = Profile.ProfileZipCode;
                RowArray[24] = Profile.ThirdPartyChannelName;
                RowArray[25] = Profile.ThirdPartyProfileID;
                RowArray[26] = Profile.ThirdPartyURL;
                RowArray[27] = Profile.ProfileTollFreeNumber;
                RowArray[28] = Profile.ProfileImageUrl;
                ProfileRow.ItemArray = RowArray;
                ProfileTable.Rows.Add(ProfileRow);
            }
        }

        private ProfileData GetProfile(DataRow RowProfile)
        {
            var ProfileData = new ProfileData();
            ProfileData.Address1 = (RowProfile["ProfileAddress1"].ToString().Replace("\"", "\"\"") ?? "");
            ProfileData.Address2 = (RowProfile["ProfileAddress2"].ToString().Replace("\"", "\"\"") ?? "");
            ProfileData.BusinessType = (RowProfile["ProfileBusinessType"].ToString().Replace("\"", "\"\"") ?? "");
            ProfileData.City = (RowProfile["ProfileCity"].ToString().Replace("\"", "\"\"") ?? "");
            ProfileData.DirectoryID = (RowProfile["DirectoryID"].ToString().Replace("\"", "\"\"") ?? "");
            ProfileData.DirectoryName = (RowProfile["DirectoryName"].ToString().Replace("\"", "\"\"") ?? "");
            ProfileData.ID = (RowProfile["ProfileID"].ToString().Replace("\"", "\"\"") ?? "");
            ProfileData.Latitude = (RowProfile["ProfileLatitude"].ToString().Replace("\"", "\"\"") ?? "");
            ProfileData.Longitude = (RowProfile["ProfileLongitude"].ToString().Replace("\"", "\"\"") ?? "");
            ProfileData.Name = (RowProfile["ProfileName"].ToString().Replace("\"", "\"\"") ?? "");
            ProfileData.Phone = (RowProfile["ProfilePhone"].ToString().Replace("\"", "\"\"") ?? "");
            ProfileData.State = (RowProfile["ProfileState"].ToString().Replace("\"", "\"\"") ?? "");
            ProfileData.ZipCode = (RowProfile["ProfileZipCode"].ToString().Replace("\"", "\"\"") ?? "");
            ProfileData.ThirdPartyChannel = (TPMEntities.Enums.ThirdPartyChannel)int.Parse(RowProfile["pcn_chn_ID"].ToString());
            ProfileData.ThirdPartyChannelName = (RowProfile["ThirdPartyChannelName"].ToString().Replace("\"", "\"\"") ?? "");
            ProfileData.ThirdPartyProfileID = RowProfile["ThirdPartyProfileID"].ToString();
            ProfileData.StartDateMetrics = DateTime.Parse(ConfigurationManager.AppSettings["StartDateMetrics"]);
            ProfileData.EndDateMetrics = DateTime.Parse(ConfigurationManager.AppSettings["EndDateMetrics"]);
            ProfileData.ProcessStartDate = this.ProcessStartDate;

            return ProfileData;
        }

        public static DirectoryProfilesAPIData GetDirectoryProfilesAPIData(Tracer Tracer)
        {
            DirectoryProfilesAPIData DirectoryProfilesAPIDataResult = new DirectoryProfilesAPIData();

            try
            {
                //Tries to get a token for API call
                string DirectoryIDDataAPITokenCalculated = GetDirectoryIDsToken(Tracer);
                if (DirectoryIDDataAPITokenCalculated == "")
                {
                    return new DirectoryProfilesAPIData();
                }

                var httpWebRequest = (HttpWebRequest)WebRequest.Create(string.Format(DirectoryProfilesAPIURL));
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "GET";
                httpWebRequest.Headers["Token"] = DirectoryIDDataAPITokenCalculated;
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                var result = "";
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    result = streamReader.ReadToEnd();
                }
                DirectoryProfilesAPIDataResult = JsonConvert.DeserializeObject<DirectoryProfilesAPIData>(result);

                string LogMessage = string.Format("Finished getting directory ID data");

                //CloudWatch error log message
                if (LogLevel == 2)
                {
                    Tracer.AddLogEvent(new TracerParameter(LogMessage, "", System.Environment.MachineName, "", "", false, -1));
                }
            }
            catch (Exception e)
            {
                string LogMessage = string.Format("Error getting directory profiles API data, error = {0}", e.Message);
                Log.LogMessage(LogMessage, ConfigurationManager.AppSettings["LogFileErrorName"]);

                //CloudWatch error log message
                if (LogLevel == 1 || LogLevel == 2)
                {
                    Tracer.AddLogEvent(new TracerParameter(LogMessage, "", System.Environment.MachineName, "", "", true, -1));
                    Tracer.Dispose();
                }

                Environment.Exit(4);
            }

            return DirectoryProfilesAPIDataResult;
        }

        static string GetDirectoryIDsToken(Tracer Tracer)
        {
            try
            {
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(VelocityTokenAPIURL);
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "POST";

                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    string json = "{\"ApiKey\":\"" + VelocityTokenAPIKey + "\"}";

                    streamWriter.Write(json);
                    streamWriter.Flush();
                    streamWriter.Close();
                }

                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                var result = "";
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    result = streamReader.ReadToEnd();
                }
                var ProfileDataAPITokenResponse = JsonConvert.DeserializeObject<DirectoryIDsAPIToken>(result);
                if (ProfileDataAPITokenResponse.Success)
                {
                    return ProfileDataAPITokenResponse.Token;
                }
                else
                {
                    return String.Empty;
                }
            }
            catch (Exception e)
            {
                string LogMessage = string.Format("Error getting token for DirectoryIDsData, error = {0}", e.Message);
                Log.LogMessage(LogMessage, ConfigurationManager.AppSettings["LogFileErrorName"]);

                //CloudWatch error log message
                if (LogLevel == 1 || LogLevel == 2)
                {
                    Tracer.AddLogEvent(new TracerParameter(LogMessage, "", System.Environment.MachineName, "", "", true, -1));
                    Tracer.Dispose();
                }

                return String.Empty;
            }
        }
    }
}
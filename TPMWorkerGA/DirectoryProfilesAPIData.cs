﻿using System.Collections.Generic;

namespace TPMWorkerGA
{
    public class DirectoryProfilesAPIData
    {
        public Status Status { get; set; }
        public List<VelocityProfile> Items { get; set; }
    }
    public class VelocityProfile
    {
        public string DirectoryID { get; set; }
        public string DirectoryName { get; set; }
        public string ID { get; set; }
        public string Name { get; set; }
        public string StoreID { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Phone { get; set; }
        public string URL { get; set; }
        public string URLShort { get; set; }
        public string ZipCode { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public string BusinessTypeID { get; set; }
        public string BusinessType { get; set; }
    }
    public class Status
    {
        public bool Success { get; set; }
        public object Error { get; set; }
    }
}

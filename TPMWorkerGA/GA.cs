﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;

using Google.Apis.Auth.OAuth2;
using Google.Apis.Analytics.v3;
using Google.Apis.Analytics.v3.Data;

using Newtonsoft.Json;
using TPMLog;
using TPMEntities;
using VelocityTracer;

namespace TPMWorkerGA
{
    public class GAMetrics
    {
        DataTable ProfilesTable = new DataTable();
        static string DirectoryProfilesAPIURL = ConfigurationManager.AppSettings["DirectoryProfilesAPIURL"];
        static string VelocityTokenAPIURL = ConfigurationManager.AppSettings["VelocityTokenAPIURL"];
        static string VelocityTokenAPIKey = ConfigurationManager.AppSettings["VelocityTokenAPIKey"];
        public static int LogLevel = int.Parse(ConfigurationManager.AppSettings["LogLevel"]);
        public static string LogMessage = "";
        public static string ErrorMessage = "";

        public static DirectoryProfilesAPIData GetDirectoryProfilesAPIData(string DirectoryID, Log ErrorLog, Tracer Tracer)
        {
            DirectoryProfilesAPIData DirectoryProfilesAPIDataResult = new DirectoryProfilesAPIData();

            try
            {
                //Tries to get a token for API call
                string DirectoryIDDataAPITokenCalculated = GetDirectoryIDsToken(ErrorLog, Tracer);
                if (DirectoryIDDataAPITokenCalculated == "")
                {
                    return new DirectoryProfilesAPIData();
                }

                var httpWebRequest = (HttpWebRequest)WebRequest.Create(string.Format(DirectoryProfilesAPIURL, DirectoryID));
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "GET";
                httpWebRequest.Headers["Token"] = DirectoryIDDataAPITokenCalculated;
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                var result = "";
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    result = streamReader.ReadToEnd();
                }
                DirectoryProfilesAPIDataResult = JsonConvert.DeserializeObject<DirectoryProfilesAPIData>(result);

                //CloudWatch log message
                if (LogLevel == 2)
                {
                    Tracer.AddLogEvent(new TracerParameter(LogMessage, "", System.Environment.MachineName, "", "", false, -1));
                }

                Log.LogMessage(LogMessage, ConfigurationManager.AppSettings["LogFileName"]);
            }
            catch (Exception e)
            {
                ErrorMessage = "Error getting directory data from Velocity API = " + (e.Message.Replace("\"", "\"\"") ?? "");

                //CloudWatch log message
                if (LogLevel == 1 || LogLevel == 2)
                {
                    Tracer.AddLogEvent(new TracerParameter(ErrorMessage, "", System.Environment.MachineName, "", "", true, -1));
                    Tracer.Dispose();
                }

                ErrorLog.AccumulatedLogMessage += "\"" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "\",\"GoogleAnalytics\",\"" + (DirectoryID ?? "") + "\",\"\",\"\",\"Error getting directory data from Velocity API = " + (e.Message.Replace("\"", "\"\"") ?? "") + "\", \"TPMWorkerGA\"\r\n";
                ErrorLog.ReportError(ConfigurationManager.AppSettings["LogFileErrorName"], (DirectoryID ?? ""), "", "TPMWorkerGA");
                Environment.Exit(10);
            }

            return DirectoryProfilesAPIDataResult;
        }

        static string GetDirectoryIDsToken(Log ErrorLog, Tracer Tracer)
        {
            try
            {
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(VelocityTokenAPIURL);
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "POST";

                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    string json = "{\"ApiKey\":\"" + VelocityTokenAPIKey + "\"}";

                    streamWriter.Write(json);
                    streamWriter.Flush();
                    streamWriter.Close();
                }

                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                var result = "";
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    result = streamReader.ReadToEnd();
                }
                var ProfileDataAPITokenResponse = JsonConvert.DeserializeObject<DirectoryIDsAPIToken>(result);
                if (ProfileDataAPITokenResponse.Success)
                {
                    return ProfileDataAPITokenResponse.Token;
                }
                else
                {
                    return String.Empty;
                }
            }
            catch (Exception e)
            {
                //CloudWatch log message
                if (LogLevel == 1 || LogLevel == 2)
                {
                    Tracer.AddLogEvent(new TracerParameter("Error getting token for DirectoryIDsData, error = " + (e.Message.Replace("\"", "\"\"") ?? ""), "", System.Environment.MachineName, "", "", true, -1));
                    Tracer.Dispose();
                }

                ErrorLog.AccumulatedLogMessage += "\"" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "\",\"GoogleAnalytics\",\"\",\"\",\"\",\"Error getting token for DirectoryIDsData\", \"TPMWorkerGA\"\r\n";
                Environment.Exit(10);
                return String.Empty;
            }
        }

        public void PopulateProfilesTable(DirectoryProfilesAPIData DirectoryProfilesAPIDataResult, Log ErrorLog, Tracer Tracer)
        {
            try
            {
                ProfilesTable = new DataTable();
                ProfilesTable.Columns.Add("DirectoryID", typeof(string));
                ProfilesTable.Columns.Add("DirectoryName", typeof(string));
                ProfilesTable.Columns.Add("ID", typeof(string));
                ProfilesTable.Columns.Add("Name", typeof(string));
                ProfilesTable.Columns.Add("StoreID", typeof(string));
                ProfilesTable.Columns.Add("Address1", typeof(string));
                ProfilesTable.Columns.Add("Address2", typeof(string));
                ProfilesTable.Columns.Add("Phone", typeof(string));
                ProfilesTable.Columns.Add("URL", typeof(string));
                ProfilesTable.Columns.Add("URLShort", typeof(string));
                ProfilesTable.Columns.Add("ZipCode", typeof(string));
                ProfilesTable.Columns.Add("Latitude", typeof(string));
                ProfilesTable.Columns.Add("Longitude", typeof(string));
                ProfilesTable.Columns.Add("State", typeof(string));
                ProfilesTable.Columns.Add("Country", typeof(string));
                ProfilesTable.Columns.Add("City", typeof(string));
                ProfilesTable.Columns.Add("BusinessTypeID", typeof(string));
                ProfilesTable.Columns.Add("BusinessType", typeof(string));

                foreach (VelocityProfile Profile in DirectoryProfilesAPIDataResult.Items)
                {
                    DataRow ProfileRow = ProfilesTable.NewRow();
                    object[] RowArray = new object[18];
                    RowArray[0] = Profile.DirectoryID;
                    RowArray[1] = Profile.DirectoryName;
                    RowArray[2] = Profile.ID;
                    RowArray[3] = Profile.Name;
                    RowArray[4] = Profile.StoreID;
                    RowArray[5] = Profile.Address1;
                    RowArray[6] = Profile.Address2;
                    RowArray[7] = Profile.Phone;
                    RowArray[8] = Profile.URL;
                    RowArray[9] = Profile.URLShort;
                    RowArray[10] = Profile.ZipCode;
                    RowArray[11] = Profile.Latitude;
                    RowArray[12] = Profile.Longitude;
                    RowArray[13] = Profile.State;
                    RowArray[14] = Profile.Country;
                    RowArray[15] = Profile.City;
                    RowArray[16] = Profile.BusinessTypeID;
                    RowArray[17] = Profile.BusinessType;
                    ProfileRow.ItemArray = RowArray;
                    ProfilesTable.Rows.Add(ProfileRow);
                }
            }
            catch (Exception e)
            {
                ErrorMessage = "An error occurred trying to get profiles from Velocity API = " + (e.Message.Replace("\"", "\"\"") ?? "");

                //CloudWatch log message
                if (LogLevel == 1 || LogLevel == 2)
                {
                    Tracer.AddLogEvent(new TracerParameter(ErrorMessage, "", System.Environment.MachineName, "", "", true, -1));
                }

                ErrorLog.AccumulatedLogMessage += "\"" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "\",\"GoogleAnalytics\",\"\",\"\",\"\",\"An error occurred trying to get profiles from Velocity API = " + (e.Message.Replace("\"", "\"\"") ?? "") + "\", \"TPMWorkerGA\"\r\n";
                ErrorLog.ReportError(ConfigurationManager.AppSettings["LogFileErrorName"], "", "", "TPMWorkerGA");
            }
        }

        public List<GAMetricAvailable> GetMetricsList(Log ErrorLog, Tracer Tracer)
        {
            string GAPrivateKey = ConfigurationManager.AppSettings["GAPrivateKey"].Replace(@"\n", "\n");
            var GAServiceAccountEmail = ConfigurationManager.AppSettings["GAServiceAccountEmail"];
            var GAUserAccountEmail = ConfigurationManager.AppSettings["GAUserAccountEmail"];

            //Google Authentication
            ServiceAccountCredential credential = new ServiceAccountCredential(
                new ServiceAccountCredential.Initializer(GAServiceAccountEmail)
                {
                    Scopes = new[] { AnalyticsService.Scope.AnalyticsReadonly },
                    User = GAUserAccountEmail

                }.FromPrivateKey(GAPrivateKey));

            //Gets access token
            string AccessToken = "";
            try
            {
                AccessToken = credential.GetAccessTokenForRequestAsync().Result;
            }
            catch (Exception e)
            {
                ErrorMessage = "An error occurred getting access token for metrics list = " + (e.Message.Replace("\"", "\"\"") ?? "");

                //CloudWatch log message
                if (LogLevel == 1 || LogLevel == 2)
                {
                    Tracer.AddLogEvent(new TracerParameter(ErrorMessage, "", System.Environment.MachineName, "", "", true, -1));
                    Tracer.Dispose();
                }

                ErrorLog.AccumulatedLogMessage += "\"" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "\",\"GoogleAnalytics\",\"\",\"\",\"\",\"An error occurred getting access token for metrics list = " + (e.Message.Replace("\"", "\"\"") ?? "") + "\", \"TPMWorkerGA\"\r\n";
                ErrorLog.ReportError(ConfigurationManager.AppSettings["LogFileErrorName"], "", "", "TPMWorkerGA");
                Environment.Exit(11);
            }

            //Creates the service.
            var Service = new Google.Apis.Analytics.v3.AnalyticsService(new Google.Apis.Services.BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = ConfigurationManager.AppSettings["GAApplicationName"]
            });

            Columns response = null;
            var request = Service.Metadata.Columns.List("ga");
            response = request.Execute();

            List<GAMetricAvailable> MetricsList = new List<GAMetricAvailable>();
            List<GAMetricAvailable> EventsList = new List<GAMetricAvailable>();
            foreach (Column Item in response.Items)
            {
                string MetricType = "";
                string MetricGroup = "";
                string MetricID = Item.Id;
                foreach (var Attribute in Item.Attributes)
                {
                    if (Attribute.Key == "type")
                    {
                        MetricType = Attribute.Value;
                    }
                    if (Attribute.Key == "group")
                    {
                        MetricGroup = Attribute.Value;
                    }
                }
                //Checks if attribute is metric
                if (MetricType == "METRIC")
                {
                    MetricsList.Add(new GAMetricAvailable()
                    {
                        MetricName = Item.Id,
                        MetricGroup = MetricGroup
                    });

                    //Checks if attribute is an Event
                    if (MetricGroup == "Event Tracking")
                    {
                        EventsList.Add(new GAMetricAvailable()
                        {
                            MetricName = Item.Id,
                            MetricGroup = MetricGroup
                        });
                    }
                }
            }

            return MetricsList;
        }

        public List<GAAccount> GetAllAccounts(Log ErrorLog, Tracer Tracer)
        {
            string GAPrivateKey = ConfigurationManager.AppSettings["GAPrivateKey"].Replace(@"\n", "\n");
            var GAServiceAccountEmail = ConfigurationManager.AppSettings["GAServiceAccountEmail"];
            var GAUserAccountEmail = ConfigurationManager.AppSettings["GAUserAccountEmail"];

            //Google Authentication
            ServiceAccountCredential credential = new ServiceAccountCredential(
                new ServiceAccountCredential.Initializer(GAServiceAccountEmail)
                {
                    Scopes = new[] { AnalyticsService.Scope.AnalyticsReadonly },
                    User = GAUserAccountEmail

                }.FromPrivateKey(GAPrivateKey));

            //Gets access token
            string AccessToken = "";
            try
            {
                AccessToken = credential.GetAccessTokenForRequestAsync().Result;
            }
            catch (Exception e)
            {
                ErrorMessage = "An error occurred getting access token for list of accounts = " + (e.Message.Replace("\"", "\"\"") ?? "");

                //CloudWatch log message
                if (LogLevel == 1 || LogLevel == 2)
                {
                    Tracer.AddLogEvent(new TracerParameter(ErrorMessage, "", System.Environment.MachineName, "", "", true, -1));
                    Tracer.Dispose();
                }

                ErrorLog.AccumulatedLogMessage += "\"" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "\",\"GoogleAnalytics\",\"\",\"\",\"\",\"An error occurred getting access token for list of accounts = " + (e.Message.Replace("\"", "\"\"") ?? "") + "\", \"TPMWorkerGA\"\r\n";
                ErrorLog.ReportError(ConfigurationManager.AppSettings["LogFileErrorName"], "", "", "TPMWorkerGA");
                Environment.Exit(12);
            }

            //Creates the service.
            var Service = new Google.Apis.Analytics.v3.AnalyticsService(new Google.Apis.Services.BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = ConfigurationManager.AppSettings["GAApplicationName"]
            });

            Accounts response = null;
            var request = Service.Management.Accounts.List();
            response = request.Execute();

            List<GAAccount> AccountsList = new List<GAAccount>();
            foreach (Account Item in response.Items)
            {
                AccountsList.Add(new GAAccount()
                {
                    AccountID = Item.Id
                });
            }

            return AccountsList;
        }

        public List<GAProfile> GetAllProfiles(GAAccount Account, Log ErrorLog, Tracer Tracer)
        {
            LogMessage = string.Format("Getting metrics data for Directory ID {0}", Account.AccountID);

            //CloudWatch log message
            if (LogLevel == 2)
            {
                Tracer.AddLogEvent(new TracerParameter(LogMessage, "", System.Environment.MachineName, "", "", false, -1));
            }

            Log.LogMessage(LogMessage, ConfigurationManager.AppSettings["LogFileName"]);
            string GAPrivateKey = ConfigurationManager.AppSettings["GAPrivateKey"].Replace(@"\n", "\n");
            var GAServiceAccountEmail = ConfigurationManager.AppSettings["GAServiceAccountEmail"];
            var GAUserAccountEmail = ConfigurationManager.AppSettings["GAUserAccountEmail"];

            //Google Authentication
            ServiceAccountCredential credential = new ServiceAccountCredential(
                new ServiceAccountCredential.Initializer(GAServiceAccountEmail)
                {
                    Scopes = new[] { AnalyticsService.Scope.AnalyticsReadonly },
                    User = GAUserAccountEmail

                }.FromPrivateKey(GAPrivateKey));

            //Gets access token
            string AccessToken = "";
            try
            {
                AccessToken = credential.GetAccessTokenForRequestAsync().Result;
            }
            catch (Exception e)
            {
                ErrorMessage = "An error occurred getting access token for account ID = " + Account.AccountID + ", error = " + (e.Message.Replace("\"", "\"\"") ?? "");

                //CloudWatch log message
                if (LogLevel == 1 || LogLevel == 2)
                {
                    Tracer.AddLogEvent(new TracerParameter(ErrorMessage, "", System.Environment.MachineName, "", "", true, -1));
                    Tracer.Dispose();
                }

                ErrorLog.AccumulatedLogMessage += "\"" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "\",\"GoogleAnalytics\",\"\",\"\",\"\",\"An error occurred getting access token for account ID = " + Account.AccountID + ", error = " + (e.Message.Replace("\"", "\"\"") ?? "") + "\", \"TPMWorkerGA\"\r\n";
                ErrorLog.ReportError(ConfigurationManager.AppSettings["LogFileErrorName"], "", "", "TPMWorkerGA");
                Environment.Exit(13);
            }

            //Creates the service.
            var Service = new Google.Apis.Analytics.v3.AnalyticsService(new Google.Apis.Services.BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = ConfigurationManager.AppSettings["GAApplicationName"]
            });

            Profiles response = null;

            try
            {
                var request = Service.Management.Profiles.List("67792727", Account.AccountID);
                response = request.Execute();
            }
            catch (Exception e1)
            {
                try
                {
                    var request = Service.Management.Profiles.List("6382132", Account.AccountID);
                    response = request.Execute();
                }
                catch (Exception e2)
                {
                    ErrorMessage = "An error occurred getting profiles list = " + (e2.Message.Replace("\"", "\"\"") ?? "");

                    //CloudWatch log message
                    if (LogLevel == 1 || LogLevel == 2)
                    {
                        Tracer.AddLogEvent(new TracerParameter(ErrorMessage, "", System.Environment.MachineName, "", "", true, -1));
                        Tracer.Dispose();
                    }

                    ErrorLog.AccumulatedLogMessage += "\"" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "\",\"GoogleAnalytics\",\"" + (Account.AccountID ?? "") + "\",\"\",\"\",\"An error occurred getting profiles list = " + (e2.Message.Replace("\"", "\"\"") ?? "") + "\", \"TPMWorkerGA\"\r\n";
                    ErrorLog.ReportError(ConfigurationManager.AppSettings["LogFileErrorName"], (Account.AccountID ?? ""), "", "TPMWorkerGA");
                    Environment.Exit(14);
                }
            }

            List<GAProfile> ProfilesList = new List<GAProfile>();
            foreach (Profile Item in response.Items)
            {
                ProfilesList.Add(new GAProfile()
                {
                    ProfileID = Item.Id
                });
            }

            return ProfilesList;
        }

        public void GetAllProfileMetrics(GAProfile GAProfileData, Log ErrorLog, Tracer Tracer)
        {
            LogMessage = string.Format("Getting metrics data for directory = {0}, profile {1}", GAProfileData.DirectoryID, GAProfileData.ProfileID);

            //CloudWatch log message
            if (LogLevel == 2)
            {
                Tracer.AddLogEvent(new TracerParameter(LogMessage, "", System.Environment.MachineName, "", "", false, -1));
            }

            Log.LogMessage(LogMessage, ConfigurationManager.AppSettings["LogFileName"]);
            string GAPrivateKey = ConfigurationManager.AppSettings["GAPrivateKey"].Replace(@"\n", "\n");
            var GAServiceAccountEmail = ConfigurationManager.AppSettings["GAServiceAccountEmail"];
            var GAUserAccountEmail = ConfigurationManager.AppSettings["GAUserAccountEmail"];
            //Google Authentication
            ServiceAccountCredential credential = new ServiceAccountCredential(
                new ServiceAccountCredential.Initializer(GAServiceAccountEmail)
                {
                    Scopes = new[] { AnalyticsService.Scope.AnalyticsReadonly },
                    User = GAUserAccountEmail

                }.FromPrivateKey(GAPrivateKey));

            //Gets access token
            string AccessToken = "";
            try
            {
                AccessToken = credential.GetAccessTokenForRequestAsync().Result;
            }
            catch (Exception e)
            {
                //CloudWatch log message
                if (LogLevel == 1 || LogLevel == 2)
                {
                    ErrorMessage = "An error occurred trying to generate Google Access Token = " + (e.Message.Replace("\"", "\"\"") ?? "");
                    Tracer.AddLogEvent(new TracerParameter(ErrorMessage, "", System.Environment.MachineName, "", "", true, -1));
                    Tracer.Dispose();
                }

                ErrorLog.AccumulatedLogMessage += "\"" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "\",\"GoogleAnalytics\",\"" + (GAProfileData.DirectoryID ?? "") + "\",\"" + (GAProfileData.ProfileID ?? "") + "\",\"\",\"An error occurred trying to generate Google Access Token = " + (e.Message.Replace("\"", "\"\"") ?? "") + "\", \"TPMWorkerGA\"\r\n";
                ErrorLog.ReportError(ConfigurationManager.AppSettings["LogFileErrorName"], (GAProfileData.DirectoryID ?? ""), (GAProfileData.ProfileID ?? ""), "TPMWorkerGA");
                Environment.Exit(15);
            }

            //Creates the service.
            var Service = new Google.Apis.Analytics.v3.AnalyticsService(new Google.Apis.Services.BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = ConfigurationManager.AppSettings["GAApplicationName"]
            });

            //Starts creating CSV file
            string CSVContents = "";
            string TemporaryFilename = "ThirdPartyMetrics_" + GAProfileData.DirectoryID + "_" + DateTime.Now.ToString("yyyy-MM-dd_HH_mm_ss") + ".csv";
            string FileName = TemporaryFilename;
            TemporaryFilename = TemporaryFilename.Replace("/", "_");
            TemporaryFilename = ConfigurationManager.AppSettings["CSVFolder"] + "\\" + TemporaryFilename;
            var KeepCSVFiles = bool.Parse(ConfigurationManager.AppSettings["KeepTemporaryCSVFile"]);
            string CSVHeaders = "Date, DirectoryID, DirectoryName, MetricName, MetricValue, ProfileAddress1, ProfileAddress2, ProfileBusinessType, ProfileCity, ProfileID, ProfileLatitude, ProfileLongitude, ProfileName, ProfilePhone, ProfileState, ProfileWebsite, ProfileZipCode, ThirdPartyChannelName, ThirdPartyProfileID, ThirdPartyURL";
            File.AppendAllLines(TemporaryFilename, new List<string>() { CSVHeaders });

            //First get generic metrics
            GaData response = null;
            dynamic request = null;
            try
            {
                //Start date provided by SQS message
                request = Service.Data.Ga.Get("ga:" + GAProfileData.ProfileID, GAProfileData.StartDate.ToString("yyy-MM-dd"), GAProfileData.StartDate.ToString("yyyy-MM-dd"), "ga:bounceRate,ga:pageValue,ga:entrances,ga:pageviews,ga:uniquePageviews,ga:avgTimeOnPage,ga:exitRate");

                //Start date calculated (-1 day current date)
                //request = Service.Data.Ga.Get("ga:" + GAProfileData.ProfileID, GAProfileData.EndDate.AddDays(-1).ToString("yyy-MM-dd"), GAProfileData.EndDate.ToString("yyyy-MM-dd"), "ga:bounceRate,ga:pageValue,ga:entrances,ga:pageviews,ga:uniquePageviews,ga:avgTimeOnPage,ga:exitRate");
                request.MaxResults = 1000;
                request.StartIndex = 1;
                request.Dimensions = "ga:date,ga:landingPagePath";

                // Prepare the next page of results             
                //request.StartIndex = request.StartIndex + request.MaxResults;

                //Waits to avoid API quota exceed
                System.Threading.Thread.Sleep(Int32.Parse(ConfigurationManager.AppSettings["GAWaitBetweenRequests"]));

                //Sends GoogleAnalytics API request metric to CloudWatch
                MetricParameter metric = new MetricParameter("GoogleAnalyticsAPIRequest", 1, MetricUnit.Count,
                    new List<Dimension>() {
                    new Dimension("Name","DirectoryID"),
                    new Dimension("Value",String.Format("{0}", GAProfileData.DirectoryID))
                });
                Tracer.AddMetric(metric);
                Tracer.SendMetric(ConfigurationManager.AppSettings["MetricNameSpace"]);

                response = request.Execute();
            }
            catch (Exception e)
            {
                //CloudWatch log message
                if (LogLevel == 1 || LogLevel == 2)
                {
                    ErrorMessage = "An error occurred trying to request the analytics with metrics = ga:bounceRate,ga:pageValue,ga:entrances,ga:pageviews,ga:uniquePageviews,ga:avgTimeOnPage,ga:exitRate, and dimensions = ga:date,ga:landingPagePath" + (e.Message.Replace("\"", "\"\"") ?? "");
                    Tracer.AddLogEvent(new TracerParameter(ErrorMessage, "", System.Environment.MachineName, "", "", true, -1));
                    Tracer.Dispose();
                }

                ErrorLog.AccumulatedLogMessage += "\"" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "\",\"GoogleAnalytics\",\"" + (GAProfileData.DirectoryID ?? "") + "\",\"" + (GAProfileData.ProfileID ?? "") + "\",\"\",\"An error occurred trying to request the analytics with metrics = ga:bounceRate,ga:pageValue,ga:entrances,ga:pageviews,ga:uniquePageviews,ga:avgTimeOnPage,ga:exitRate, and dimensions = ga:date,ga:landingPagePath" + (e.Message.Replace("\"", "\"\"") ?? "") + "\", \"TPMWorkerGA\"\r\n";
                ErrorLog.ReportError(ConfigurationManager.AppSettings["LogFileErrorName"], (GAProfileData.DirectoryID ?? ""), (GAProfileData.ProfileID ?? ""), "TPMWorkerGA");
                Environment.Exit(15);
            }
            

            //Loops through all results
            int PageNumber = 0;
            while (response.Rows != null)
            {
                PageNumber++;
                foreach (var Row in response.Rows)
                {
                    string MetricDateString = Row[0];
                    string ProfileID = Row[1];
                    string MetricGABounceRate = Row[2];
                    string MetricGAPageValue = Row[3];
                    string MetricGAEntrances = Row[4];
                    string MetricGAPageviews = Row[5];
                    string MetricGAUniquePageviews = Row[6];
                    string MetricGAAvgTimeOnPage = Row[7];
                    string MetricGAExitRate = Row[8];

                    //Checks if need to skip profiles with special names
                    if (ConfigurationManager.AppSettings["SkipSpecialNamesProfiles"] == "1")
                    {
                        if (ProfileID == "" || ProfileID == "/" || ProfileID.IndexOf("?") > 0 || ProfileID == "(not set)")
                        {
                            continue;
                        }
                    }

                    DateTime MetricDate = DateTime.ParseExact(MetricDateString, "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture);

                    //Gets profile data to include in CSV
                    ProfileData TestProfile = GetProfileData(GAProfileData.DirectoryID, ProfileID, ErrorLog, Tracer);

                    //Prepares all strings to be saved in CSV format
                    string TestProfileID = "";
                    if (TestProfile.ID != null)
                    {
                        TestProfileID = TestProfile.ID.Replace("\"", "\"\"");
                    }

                    //Avoids creation of CSV for a profile we didn't find
                    if (TestProfileID == "")
                    {
                        continue;
                    }

                    string TestProfileDirectoryID = "";
                    if (TestProfile.DirectoryID != null) {
                        TestProfileDirectoryID = TestProfile.DirectoryID.Replace("\"", "\"\"");
                    }
                    string TestProfileDirectoryName = "";
                    if (TestProfile.DirectoryName != null)
                    {
                        TestProfileDirectoryName = TestProfile.DirectoryName.Replace("\"", "\"\"");
                    }
                    string TestProfileAddress1 = "";
                    if (TestProfile.Address1 != null)
                    {
                        TestProfileAddress1 = TestProfile.Address1.Replace("\"", "\"\"");
                    }
                    string TestProfileAddress2 = "";
                    if (TestProfile.Address2 != null)
                    {
                        TestProfileAddress2 = TestProfile.Address2.Replace("\"", "\"\"");
                    }
                    string TestProfileBusinessType = "";
                    if (TestProfile.BusinessType != null)
                    {
                        TestProfileBusinessType = TestProfile.BusinessType.Replace("\"", "\"\"");
                    }
                    string TestProfileCity = "";
                    if (TestProfile.City != null)
                    {
                        TestProfileCity = TestProfile.City.Replace("\"", "\"\"");
                    }
                    string TestProfileLatitude = "";
                    if (TestProfile.Latitude != null)
                    {
                        TestProfileLatitude = TestProfile.Latitude.Replace("\"", "\"\"");
                    }
                    string TestProfileLongitude = "";
                    if (TestProfile.Longitude != null)
                    {
                        TestProfileLongitude = TestProfile.Longitude.Replace("\"", "\"\"");
                    }
                    string TestProfileName = "";
                    if (TestProfile.Name != null)
                    {
                        TestProfileName = TestProfile.Name.Replace("\"", "\"\"");
                    }
                    string TestProfilePhone = "";
                    if (TestProfile.Phone != null)
                    {
                        TestProfilePhone = TestProfile.Phone.Replace("\"", "\"\"");
                    }
                    string TestProfileState = "";
                    if (TestProfile.State != null)
                    {
                        TestProfileState = TestProfile.State.Replace("\"", "\"\"");
                    }
                    string TestProfileWebsite = "";
                    if (TestProfile.Website != null)
                    {
                        TestProfileWebsite = TestProfile.Website.Replace("\"", "\"\"");
                    }
                    string TestProfileZipCode = "";
                    if (TestProfile.ZipCode != null)
                    {
                        TestProfileZipCode = TestProfile.ZipCode.Replace("\"", "\"\"");
                    }
                    string TestProfileThirdPartyChannelName = "";
                    if (TestProfile.ThirdPartyChannelName != null)
                    {
                        TestProfileThirdPartyChannelName = TestProfile.ThirdPartyChannelName.Replace("\"", "\"\"");
                    }
                    string TestProfileThirdPartyProfileID = "";
                    if (TestProfile.ThirdPartyProfileID != null)
                    {
                        TestProfileThirdPartyProfileID = TestProfile.ThirdPartyProfileID.Replace("\"", "\"\"");
                    }

                    //MetricGABounceRate row
                    CSVContents = "\"" + MetricDate.ToString("yyyy-MM-dd HH:mm:ss") + "\", \"" + TestProfileDirectoryID + "\", \"" + TestProfileDirectoryName + "\", \"Bounce Rate\", \"" + MetricGABounceRate + "\", \"" + TestProfileAddress1 + "\", \"" + TestProfileAddress2 + "\", \"" + TestProfileBusinessType + "\", \"" + TestProfileCity + "\", \"" + TestProfileID + "\", \"" + TestProfileLatitude + "\", \"" + TestProfileLongitude + "\", \"" + TestProfileName + "\", \"" + TestProfilePhone + "\", \"" + TestProfileState + "\", \"" + TestProfileWebsite + "\", \"" + TestProfileZipCode + "\", \"" + TestProfileThirdPartyChannelName + "\", \"ga:" + TestProfileThirdPartyProfileID + "\", \"" + TestProfileThirdPartyProfileID + "\"";

                    File.AppendAllLines(TemporaryFilename, new List<string>() { CSVContents });

                    //MetricGAPageValue row
                    CSVContents = "\"" + MetricDate.ToString("yyyy-MM-dd HH:mm:ss") + "\", \"" + TestProfileDirectoryID + "\", \"" + TestProfileDirectoryName + "\", \"Page Value\", \"" + MetricGAPageValue.Replace("\"", "\"\"") + "\", \"" + TestProfileAddress1 + "\", \"" + TestProfileAddress2 + "\", \"" + TestProfileBusinessType + "\", \"" + TestProfileCity + "\", \"" + TestProfileID + "\", \"" + TestProfileLatitude + "\", \"" + TestProfileLongitude + "\", \"" + TestProfileName + "\", \"" + TestProfilePhone + "\", \"" + TestProfileState + "\", \"" + TestProfileWebsite + "\", \"" + TestProfileZipCode + "\", \"" + TestProfileThirdPartyChannelName + "\", \"ga:" + TestProfileThirdPartyProfileID + "\", \"" + TestProfileThirdPartyProfileID + "\"";

                    File.AppendAllLines(TemporaryFilename, new List<string>() { CSVContents });

                    //MetricGAEntrances row
                    CSVContents = "\"" + MetricDate.ToString("yyyy-MM-dd HH:mm:ss") + "\", \"" + TestProfileDirectoryID + "\", \"" + TestProfileDirectoryName + "\", \"Entrances\", \"" + MetricGAEntrances.Replace("\"", "\"\"") + "\", \"" + TestProfileAddress1 + "\", \"" + TestProfileAddress2 + "\", \"" + TestProfileBusinessType + "\", \"" + TestProfileCity + "\", \"" + TestProfileID + "\", \"" + TestProfileLatitude + "\", \"" + TestProfileLongitude + "\", \"" + TestProfileName + "\", \"" + TestProfilePhone + "\", \"" + TestProfileState + "\", \"" + TestProfileWebsite + "\", \"" + TestProfileZipCode + "\", \"" + TestProfileThirdPartyChannelName + "\", \"ga:" + TestProfileThirdPartyProfileID + "\", \"" + TestProfileThirdPartyProfileID + "\"";

                    File.AppendAllLines(TemporaryFilename, new List<string>() { CSVContents });

                    //MetricGAPageViews row
                    CSVContents = "\"" + MetricDate.ToString("yyyy-MM-dd HH:mm:ss") + "\", \"" + TestProfileDirectoryID + "\", \"" + TestProfileDirectoryName + "\", \"Pageviews\", \"" + MetricGAPageviews.Replace("\"", "\"\"") + "\", \"" + TestProfileAddress1 + "\", \"" + TestProfileAddress2 + "\", \"" + TestProfileBusinessType + "\", \"" + TestProfileCity + "\", \"" + TestProfileID + "\", \"" + TestProfileLatitude + "\", \"" + TestProfileLongitude + "\", \"" + TestProfileName + "\", \"" + TestProfilePhone + "\", \"" + TestProfileState + "\", \"" + TestProfileWebsite + "\", \"" + TestProfileZipCode + "\", \"" + TestProfileThirdPartyChannelName + "\", \"ga:" + TestProfileThirdPartyProfileID + "\", \"" + TestProfileThirdPartyProfileID + "\"";

                    File.AppendAllLines(TemporaryFilename, new List<string>() { CSVContents });

                    //MetricGAUniquePageviews row
                    CSVContents = "\"" + MetricDate.ToString("yyyy-MM-dd HH:mm:ss") + "\", \"" + TestProfileDirectoryID + "\", \"" + TestProfileDirectoryName + "\", \"Unique Pageviews\", \"" + MetricGAUniquePageviews.Replace("\"", "\"\"") + "\", \"" + TestProfileAddress1 + "\", \"" + TestProfileAddress2 + "\", \"" + TestProfileBusinessType + "\", \"" + TestProfileCity + "\", \"" + TestProfileID + "\", \"" + TestProfileLatitude + "\", \"" + TestProfileLongitude + "\", \"" + TestProfileName + "\", \"" + TestProfilePhone + "\", \"" + TestProfileState + "\", \"" + TestProfileWebsite + "\", \"" + TestProfileZipCode + "\", \"" + TestProfileThirdPartyChannelName + "\", \"ga:" + TestProfileThirdPartyProfileID + "\", \"" + TestProfileThirdPartyProfileID + "\"";

                    File.AppendAllLines(TemporaryFilename, new List<string>() { CSVContents });

                    //MetricGAAvgTimeOnPage row
                    CSVContents = "\"" + MetricDate.ToString("yyyy-MM-dd HH:mm:ss") + "\", \"" + TestProfileDirectoryID + "\", \"" + TestProfileDirectoryName + "\", \"Avg. Time on Page\", \"" + MetricGAAvgTimeOnPage.Replace("\"", "\"\"") + "\", \"" + TestProfileAddress1 + "\", \"" + TestProfileAddress2 + "\", \"" + TestProfileBusinessType + "\", \"" + TestProfileCity + "\", \"" + TestProfileID + "\", \"" + TestProfileLatitude + "\", \"" + TestProfileLongitude + "\", \"" + TestProfileName + "\", \"" + TestProfilePhone + "\", \"" + TestProfileState + "\", \"" + TestProfileWebsite + "\", \"" + TestProfileZipCode + "\", \"" + TestProfileThirdPartyChannelName + "\", \"ga:" + TestProfileThirdPartyProfileID + "\", \"" + TestProfileThirdPartyProfileID + "\"";

                    File.AppendAllLines(TemporaryFilename, new List<string>() { CSVContents });

                    //MetricGAExitRate row
                    CSVContents = "\"" + MetricDate.ToString("yyyy-MM-dd HH:mm:ss") + "\", \"" + TestProfileDirectoryID + "\", \"" + TestProfileDirectoryName + "\", \"% Exit\", \"" + MetricGAExitRate.Replace("\"", "\"\"") + "\", \"" + TestProfileAddress1 + "\", \"" + TestProfileAddress2 + "\", \"" + TestProfileBusinessType + "\", \"" + TestProfileCity + "\", \"" + TestProfileID + "\", \"" + TestProfileLatitude + "\", \"" + TestProfileLongitude + "\", \"" + TestProfileName + "\", \"" + TestProfilePhone + "\", \"" + TestProfileState + "\", \"" + TestProfileWebsite + "\", \"" + TestProfileZipCode + "\", \"" + TestProfileThirdPartyChannelName + "\", \"ga:" + TestProfileThirdPartyProfileID + "\", \"" + TestProfileThirdPartyProfileID + "\"";

                    File.AppendAllLines(TemporaryFilename, new List<string>() { CSVContents });
                }

                // Prepare the next page of results             
                request.StartIndex = request.StartIndex + request.MaxResults;

                //Waits to avoid API quota exceed
                System.Threading.Thread.Sleep(Int32.Parse(ConfigurationManager.AppSettings["GAWaitBetweenRequests"]));

                //Sends GoogleAnalytics API request metric to CloudWatch
                MetricParameter metric = new MetricParameter("GoogleAnalyticsAPIRequest", 1, MetricUnit.Count,
                    new List<Dimension>() {
                    new Dimension("Name","DirectoryID"),
                    new Dimension("Value",String.Format("{0}", GAProfileData.DirectoryID))
                });
                Tracer.AddMetric(metric);
                Tracer.SendMetric(ConfigurationManager.AppSettings["MetricNameSpace"]);

                // Execute and process the next page request
                response = request.Execute();
            }

            //Gets all other metrics
            response = null;
            request = null;
            try
            {
                //Start date provided by SQS message
                request = Service.Data.Ga.Get("ga:" + GAProfileData.ProfileID, GAProfileData.StartDate.ToString("yyy-MM-dd"), GAProfileData.StartDate.ToString("yyyy-MM-dd"), ConfigurationManager.AppSettings["GAMetrics"]);

                //Start date calculated (-1 day current date)
                //request = Service.Data.Ga.Get("ga:" + GAProfileData.ProfileID, GAProfileData.EndDate.AddDays(-1).ToString("yyy-MM-dd"), GAProfileData.EndDate.ToString("yyyy-MM-dd"), ConfigurationManager.AppSettings["GAMetrics"]);
                request.MaxResults = 1000;
                request.StartIndex = 1;
                request.Dimensions = ConfigurationManager.AppSettings["GADimensions"];

                //Waits to avoid API quota exceed
                System.Threading.Thread.Sleep(Int32.Parse(ConfigurationManager.AppSettings["GAWaitBetweenRequests"]));

                //Sends GoogleAnalytics API request metric to CloudWatch
                MetricParameter metric = new MetricParameter("GoogleAnalyticsAPIRequest", 1, MetricUnit.Count,
                    new List<Dimension>() {
                    new Dimension("Name","DirectoryID"),
                    new Dimension("Value",String.Format("{0}", GAProfileData.DirectoryID))
                });
                Tracer.AddMetric(metric);
                Tracer.SendMetric(ConfigurationManager.AppSettings["MetricNameSpace"]);

                response = request.Execute();
            }
            catch (Exception e)
            {
                //CloudWatch log message
                if (LogLevel == 1 || LogLevel == 2)
                {
                    ErrorMessage = "An error occurred trying to request the analytics with metrics = ga:bounceRate,ga:pageValue,ga:entrances,ga:pageviews,ga:uniquePageviews,ga:avgTimeOnPage,ga:exitRate, and dimensions = ga:date,ga:landingPagePath" + (e.Message.Replace("\"", "\"\"") ?? "");
                    Tracer.AddLogEvent(new TracerParameter(ErrorMessage, "", System.Environment.MachineName, "", "", true, -1));
                    Tracer.Dispose();
                }

                ErrorLog.AccumulatedLogMessage += "\"" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "\",\"GoogleAnalytics\",\"" + (GAProfileData.DirectoryID ?? "") + "\",\"" + (GAProfileData.ProfileID ?? "") + "\",\"\",\"An error occurred trying to request the analytics with metrics = ga:bounceRate,ga:pageValue,ga:entrances,ga:pageviews,ga:uniquePageviews,ga:avgTimeOnPage,ga:exitRate, and dimensions = ga:date,ga:landingPagePath" + (e.Message.Replace("\"", "\"\"") ?? "") + "\", \"TPMWorkerGA\"\r\n";
                ErrorLog.ReportError(ConfigurationManager.AppSettings["LogFileErrorName"], (GAProfileData.DirectoryID ?? ""), (GAProfileData.ProfileID ?? ""), "TPMWorkerGA");
                Environment.Exit(15);
            }


            PageNumber = 0;
            while (response.Rows != null)
            {
                PageNumber++;
                foreach (var Row in response.Rows)
                {
                    string MetricDateString = Row[0];
                    string MetricName = Row[2];
                    string MetricValue = Row[4];
                    string ProfileID = Row[3];
                    string TotalSessions = Row[5];

                    //Checks if need to skip profiles with special names
                    if (ConfigurationManager.AppSettings["SkipSpecialNamesProfiles"] == "1")
                    {
                        if (ProfileID == "/" || ProfileID.IndexOf("?") > 0 || ProfileID == "(not set)")
                        {
                            continue;
                        }
                    }

                    DateTime MetricDate = DateTime.ParseExact(MetricDateString, "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture);

                    //Gets profile data to include in CSV
                    ProfileData TestProfile = GetProfileData(GAProfileData.DirectoryID, ProfileID, ErrorLog, Tracer);

                    //Prepares all strings to be saved in CSV format
                    string TestProfileID = "";
                    if (TestProfile.ID != null)
                    {
                        TestProfileID = TestProfile.ID.Replace("\"", "\"\"");
                    }

                    //Avoids creation of CSV for a profile we didn't find
                    if (TestProfileID == "")
                    {
                        continue;
                    }

                    string TestProfileDirectoryID = "";
                    if (TestProfile.DirectoryID != null)
                    {
                        TestProfileDirectoryID = TestProfile.DirectoryID.Replace("\"", "\"\"");
                    }
                    string TestProfileDirectoryName = "";
                    if (TestProfile.DirectoryName != null)
                    {
                        TestProfileDirectoryName = TestProfile.DirectoryName.Replace("\"", "\"\"");
                    }
                    string TestProfileAddress1 = "";
                    if (TestProfile.Address1 != null)
                    {
                        TestProfileAddress1 = TestProfile.Address1.Replace("\"", "\"\"");
                    }
                    string TestProfileAddress2 = "";
                    if (TestProfile.Address2 != null)
                    {
                        TestProfileAddress2 = TestProfile.Address2.Replace("\"", "\"\"");
                    }
                    string TestProfileBusinessType = "";
                    if (TestProfile.BusinessType != null)
                    {
                        TestProfileBusinessType = TestProfile.BusinessType.Replace("\"", "\"\"");
                    }
                    string TestProfileCity = "";
                    if (TestProfile.City != null)
                    {
                        TestProfileCity = TestProfile.City.Replace("\"", "\"\"");
                    }
                    string TestProfileLatitude = "";
                    if (TestProfile.Latitude != null)
                    {
                        TestProfileLatitude = TestProfile.Latitude.Replace("\"", "\"\"");
                    }
                    string TestProfileLongitude = "";
                    if (TestProfile.Longitude != null)
                    {
                        TestProfileLongitude = TestProfile.Longitude.Replace("\"", "\"\"");
                    }
                    string TestProfileName = "";
                    if (TestProfile.Name != null)
                    {
                        TestProfileName = TestProfile.Name.Replace("\"", "\"\"");
                    }
                    string TestProfilePhone = "";
                    if (TestProfile.Phone != null)
                    {
                        TestProfilePhone = TestProfile.Phone.Replace("\"", "\"\"");
                    }
                    string TestProfileState = "";
                    if (TestProfile.State != null)
                    {
                        TestProfileState = TestProfile.State.Replace("\"", "\"\"");
                    }
                    string TestProfileWebsite = "";
                    if (TestProfile.Website != null)
                    {
                        TestProfileWebsite = TestProfile.Website.Replace("\"", "\"\"");
                    }
                    string TestProfileZipCode = "";
                    if (TestProfile.ZipCode != null)
                    {
                        TestProfileZipCode = TestProfile.ZipCode.Replace("\"", "\"\"");
                    }
                    string TestProfileThirdPartyChannelName = "";
                    if (TestProfile.ThirdPartyChannelName != null)
                    {
                        TestProfileThirdPartyChannelName = TestProfile.ThirdPartyChannelName.Replace("\"", "\"\"");
                    }
                    string TestProfileThirdPartyProfileID = "";
                    if (TestProfile.ThirdPartyProfileID != null)
                    {
                        TestProfileThirdPartyProfileID = TestProfile.ThirdPartyProfileID.Replace("\"", "\"\"");
                    }

                    //Metric row
                    CSVContents = "\"" + MetricDate.ToString("yyyy-MM-dd HH:mm:ss") + "\", \""+ TestProfileDirectoryID + "\", \"" + TestProfileDirectoryName + "\", \"" + MetricName.Replace("\"", "\"\"") + "\", \"" + MetricValue + "\", \"" + TestProfileAddress1 + "\", \"" + TestProfileAddress2 + "\", \"" + TestProfileBusinessType + "\", \"" + TestProfileCity + "\", \"" + TestProfileID + "\", \"" + TestProfileLatitude + "\", \"" + TestProfileLongitude + "\", \"" + TestProfileName + "\", \"" + TestProfilePhone + "\", \"" + TestProfileState + "\", \"" + TestProfileWebsite + "\", \"" + TestProfileZipCode + "\", \"" + TestProfileThirdPartyChannelName + "\", \"ga:" + TestProfileThirdPartyProfileID + "\", \"" + TestProfileThirdPartyProfileID + "\"";
                    File.AppendAllLines(TemporaryFilename, new List<string>() { CSVContents });

                    //Sessions row
                    CSVContents = "\"" + MetricDate.ToString("yyyy-MM-dd HH:mm:ss") + "\", \"" + TestProfileDirectoryID + "\", \"" + TestProfileDirectoryName + "\", \"" + MetricName + "-Session\", \"" + TotalSessions + "\", \"" + TestProfileAddress1 + "\", \"" + TestProfileAddress2 + "\", \"" + TestProfileBusinessType + "\", \"" + TestProfileCity + "\", \"" + TestProfileID + "\", \"" + TestProfileLatitude + "\", \"" + TestProfileLongitude + "\", \"" + TestProfileName + "\", \"" + TestProfilePhone + "\", \"" + TestProfileState + "\", \"" + TestProfileWebsite + "\", \"" + TestProfileZipCode + "\", \"" + TestProfileThirdPartyChannelName + "\", \"ga:" + TestProfileThirdPartyProfileID + "\", \"" + TestProfileThirdPartyProfileID + "\"";
                    File.AppendAllLines(TemporaryFilename, new List<string>() { CSVContents });
                }

                // Prepare the next page of results             
                request.StartIndex = request.StartIndex + request.MaxResults;

                //Waits to avoid API quota exceed
                System.Threading.Thread.Sleep(Int32.Parse(ConfigurationManager.AppSettings["GAWaitBetweenRequests"]));

                //Sends GoogleAnalytics API request metric to CloudWatch
                MetricParameter metric = new MetricParameter("GoogleAnalyticsAPIRequest", 1, MetricUnit.Count,
                    new List<Dimension>() {
                    new Dimension("Name","DirectoryID"),
                    new Dimension("Value",String.Format("{0}", GAProfileData.DirectoryID))
                });
                Tracer.AddMetric(metric);
                Tracer.SendMetric(ConfigurationManager.AppSettings["MetricNameSpace"]);

                // Execute and process the next page request
                response = request.Execute();
            }

            //Saves local file in S3 bucket
            string BucketName = ConfigurationManager.AppSettings["S3BucketPath"];
            Boolean SaveAttempt = false;
            try
            {
                SIMAmazon AmazonBucket = new SIMAmazon();
                var S3Attempts = 0;
                var S3MaxAttempts = 5;
                while (S3Attempts < S3MaxAttempts && !SaveAttempt)
                {
                    S3Attempts++;
                    try
                    {
                        SaveAttempt = AmazonBucket.SaveToS3(BucketName, TemporaryFilename, DateTime.Now.ToString("yyyy") + "/" + DateTime.Now.ToString("MM") + "/" + DateTime.Now.ToString("dd") + "/" + "TPMWorkerGA_" + FileName, GAProfileData.StartDate, GAProfileData.DirectoryID, GAProfileData.ProfileID, ErrorLog);
                    }
                    catch (Exception)
                    {
                        if (S3Attempts >= S3MaxAttempts)
                        {
                            throw;
                        }
                        System.Threading.Thread.Sleep(1000);
                    }
                }
                if (!KeepCSVFiles)
                {
                    File.Delete(TemporaryFilename);
                }
            }
            catch (Exception e)
            {
                //CloudWatch log message
                if (LogLevel == 1 || LogLevel == 2)
                {
                    ErrorMessage = "Error creating the S3 bucket file = " + (e.Message.Replace("\"", "\"\"") ?? "");
                    Tracer.AddLogEvent(new TracerParameter(ErrorMessage, "", System.Environment.MachineName, "", "", true, -1));
                    Tracer.Dispose();
                }

                ErrorLog.AccumulatedLogMessage += "\"" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "\",\"GoogleAnalytics\",\"" + (GAProfileData.DirectoryID ?? "") + "\",\"" + (GAProfileData.ProfileID ?? "") + "\",\"\",\"Error creating the S3 bucket file = " + (e.Message.Replace("\"", "\"\"") ?? "") + "\", \"TPMWorkerGMB\"\r\n";
                ErrorLog.ReportError(ConfigurationManager.AppSettings["LogFileErrorName"], (GAProfileData.DirectoryID ?? ""), (GAProfileData.ProfileID ?? ""), "TPMWorkerGA");

                //Saves failed file in local folder
                if (!Directory.Exists(ConfigurationManager.AppSettings["CSVFolder"] + "\\" + ConfigurationManager.AppSettings["CSVFailedUploadsFolder"] + "\\"))
                {
                    Directory.CreateDirectory(ConfigurationManager.AppSettings["CSVFolder"] + "\\" + ConfigurationManager.AppSettings["CSVFailedUploadsFolder"] + "\\");
                }
                File.Move(TemporaryFilename, ConfigurationManager.AppSettings["CSVFolder"] + "\\" + ConfigurationManager.AppSettings["CSVFailedUploadsFolder"] + "\\" + FileName);
                Log.LogMessage("GAWorker exiting with code 3", ConfigurationManager.AppSettings["LogFileErrorName"]);
                Environment.Exit(16);
            }


            //CloudWatch log message
            if (LogLevel == 2)
            {
                ErrorMessage = string.Format("TPMWorkerGA finished for directory ID = {0}", GAProfileData.DirectoryID);
                Tracer.AddLogEvent(new TracerParameter(ErrorMessage, "", System.Environment.MachineName, "", "", false, -1));
                Tracer.Dispose();
            }
            
            Environment.Exit(0);
        }

        public ProfileData GetProfileData(string DirectoryID, string ProfileID, Log ErrorLog, Tracer Tracer)
        {
            if (ProfilesTable.Rows.Count == 0)
            {
                DirectoryProfilesAPIData DirectoryProfilesAPIDataResult = GetDirectoryProfilesAPIData(DirectoryID, ErrorLog, Tracer);
                PopulateProfilesTable(DirectoryProfilesAPIDataResult, ErrorLog, Tracer);
            }

            DataRow[] FoundProfileRows;

            //If ProfileID starts with /, remove it
            if (ProfileID.Substring(0,1) == "/")
            {
                ProfileID = ProfileID.Substring(1, ProfileID.Length - 1);
            }

            //If ProfileID ends with /, remove it
            if (ProfileID.Substring(ProfileID.Length - 1, 1) == "/")
            {
                ProfileID = ProfileID.Substring(0, ProfileID.Length - 1);
            }
            ProfileID = ProfileID.Replace("\'", "''");
            FoundProfileRows = ProfilesTable.Select("URLShort = '" + ProfileID  + "'");
            ProfileData ProfileInfo = new ProfileData();
            if (FoundProfileRows.Count() > 0)
            {
                ProfileInfo.DirectoryID = FoundProfileRows[0].ItemArray[0].ToString();
                ProfileInfo.DirectoryName = FoundProfileRows[0].ItemArray[1].ToString();
                ProfileInfo.ID = FoundProfileRows[0].ItemArray[2].ToString();
                ProfileInfo.Name = FoundProfileRows[0].ItemArray[3].ToString();
                ProfileInfo.Address1 = FoundProfileRows[0].ItemArray[5].ToString();
                ProfileInfo.Address2 = FoundProfileRows[0].ItemArray[6].ToString();
                ProfileInfo.Phone = FoundProfileRows[0].ItemArray[7].ToString();
                ProfileInfo.Website = FoundProfileRows[0].ItemArray[8].ToString();
                ProfileInfo.ThirdPartyProfileID = FoundProfileRows[0].ItemArray[9].ToString();
                ProfileInfo.ZipCode = FoundProfileRows[0].ItemArray[10].ToString();
                ProfileInfo.Latitude = FoundProfileRows[0].ItemArray[11].ToString();
                ProfileInfo.Longitude = FoundProfileRows[0].ItemArray[12].ToString();
                ProfileInfo.State = FoundProfileRows[0].ItemArray[13].ToString();
                ProfileInfo.City = FoundProfileRows[0].ItemArray[15].ToString();
                ProfileInfo.BusinessType = FoundProfileRows[0].ItemArray[17].ToString();
                ProfileInfo.ThirdPartyChannel = TPMEntities.Enums.ThirdPartyChannel.GoogleAnalytics;
                ProfileInfo.ThirdPartyChannelName = "Google Analytics";
                ProfileInfo.ThirdPartyURL = FoundProfileRows[0].ItemArray[9].ToString();
                ProfileInfo.ProcessStartDate = DateTime.ParseExact("2017-08-01 00:00:00", "yyyy-MM-dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
            }
            else
            {
                //CloudWatch log message
                if (LogLevel == 1 || LogLevel == 2)
                {
                    ErrorMessage = string.Format("No profile data found from Velocity API, directory ID = {0}, profileID = {1}", DirectoryID, ProfileID);
                    Tracer.AddLogEvent(new TracerParameter(ErrorMessage, "", System.Environment.MachineName, "", "", true, -1));

                    MetricParameter metric = new MetricParameter("NoProfileDataFound", 1, MetricUnit.Count,
                        new List<Dimension>() {
                        new Dimension("Name","DirectoryID"),
                        new Dimension("Value",String.Format("{0}", DirectoryID))
                    });
                    Tracer.AddMetric(metric);
                    Tracer.SendMetric(ConfigurationManager.AppSettings["MetricNameSpace"]);
                }

                //Avoids ProfileID not numeric
                int n;
                string OriginalProfileID = ProfileID;
                if (!int.TryParse(ProfileID, out n))
                {
                    ProfileID = "";
                }

                ErrorLog.AccumulatedLogMessage += "\"" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "\",\"GoogleAnalytics\",\"" + (DirectoryID ?? "") + "\",\"" + (ProfileID ?? "") + "\",\""+ OriginalProfileID + "\",\"No profile data found from Velocity API for directory " + (DirectoryID ?? "") + ", profile ID searched = " + (OriginalProfileID ?? "") + "\", \"TPMWorkerGA\"\r\n";
                ErrorLog.ReportError(ConfigurationManager.AppSettings["LogFileErrorName"], (DirectoryID ?? ""), (ProfileID ?? ""), "TPMWorkerGA");
            }

            return ProfileInfo;
        }
    }

    public class GAAccount
    {
        public string AccountID { get; set; }
    }

    public class GAProfile
    {
        public string DirectoryID { get; set; }
        public string ProfileID { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime ProcessDate { get; set; }
    }

    public class GAMetricAvailable
    {
        public string MetricName { get; set; }
        public string MetricGroup { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Configuration;

using Newtonsoft.Json;
using TPMEntities;

using TPMLog;
using VelocityTracer;
using System.Net;

namespace TPMWorkerGA
{
    class Program
    {
        static void Main(string[] args)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;

            Tracer Tracer = new Tracer("TPMWorkerGA");
            int LogLevel = int.Parse(ConfigurationManager.AppSettings["LogLevel"]);

            //CloudWatch log message
            if (LogLevel == 2)
            {
                Tracer.AddLogEvent(new TracerParameter("Starting TPMWorkerGA", "", System.Environment.MachineName, "", "", true, -1));
            }

            Log ErrorLog = new Log();

            //CloudWatch log message
            if (LogLevel == 2)
            {
                Tracer.AddLogEvent(new TracerParameter("Starting TPMWorkerGA", "", System.Environment.MachineName, "", "", false, -1));
            }

            //Checks if mandatory Metrics and Dimensions were set
            if (!CheckGAMetricsDimensions())
            {
                string GAMetrics = ConfigurationManager.AppSettings["GAMetrics"];
                string GADimensions = ConfigurationManager.AppSettings["GADimensions"];
                string ErrorMessage = string.Format("The required metrics and dimensions were not specified. Metrics used = {0}, Dimensions used = {1}, at " + DateTime.Now.ToString(), GAMetrics, GADimensions);
                Log.LogMessage(ErrorMessage, ConfigurationManager.AppSettings["LogFileErrorName"]);

                //CloudWatch log message
                if (LogLevel == 1 || LogLevel == 2)
                {
                    Tracer.AddLogEvent(new TracerParameter(ErrorMessage, "", System.Environment.MachineName, "", "", true, -1));
                    Tracer.Dispose();
                }

                Environment.Exit(17);
            }
            string ArgumentsString = string.Join("", args);
            ArgumentsString = ArgumentsString.Replace(":", "\":\"");
            ArgumentsString = ArgumentsString.Replace(",", "\",\"");
            ArgumentsString = ArgumentsString.Replace("{", "{\"");
            ArgumentsString = ArgumentsString.Replace("}", "\"}");
            ArgumentsString = ArgumentsString.Replace("\"\"", "\"");
            var QueueMessage = JsonConvert.DeserializeObject<TPMQueueMessage>(ArgumentsString);

            GAMetrics GAAllMetrics = new GAMetrics();
            try
            {
                List<GAMetricAvailable> MetricsList = GAAllMetrics.GetMetricsList(ErrorLog, Tracer);
            }
            catch (Exception e)
            {
                //CloudWatch log message
                if (LogLevel == 1 || LogLevel == 2)
                {
                    Tracer.AddLogEvent(new TracerParameter("Error getting all metrics list, error message = " + e.Message, "", System.Environment.MachineName, "", "", true, -1));
                    Tracer.Dispose();
                }
            }

            try
            {
                List<GAAccount> AccountsList = GAAllMetrics.GetAllAccounts(ErrorLog, Tracer);
            }
            catch (Exception e)
            {
                //CloudWatch log message
                if (LogLevel == 1 || LogLevel == 2)
                {
                    Tracer.AddLogEvent(new TracerParameter("Error getting all accounts, error message = " + e.Message, "", System.Environment.MachineName, "", "", true, -1));
                    Tracer.Dispose();
                }
            }

            //AccountID for Google Analytics corresponds to DirectoryID in Velocity
            try
            {
                GAAccount Account = new GAAccount();
                Account.AccountID = QueueMessage.AccountID;
                List<GAProfile> ProfilesList = GAAllMetrics.GetAllProfiles(Account, ErrorLog, Tracer);
                foreach (GAProfile GAProfile in ProfilesList)
                {
                    GAProfile.ProcessDate = DateTime.Now;
                    GAProfile.DirectoryID = QueueMessage.DirectoryID;
                    GAProfile.StartDate = Convert.ToDateTime(QueueMessage.StartDateMetrics);
                    GAProfile.EndDate = Convert.ToDateTime(QueueMessage.EndDateMetrics);
                    GAAllMetrics.GetAllProfileMetrics(GAProfile, ErrorLog, Tracer);
                }
            }
            catch (Exception e2)
            {
                GAAccount Account = new GAAccount();
                Account.AccountID = QueueMessage.AccountID;
                string ErrorMessage = "";
                ErrorMessage = "An error occurred getting profiles list = " + (e2.Message.Replace("\"", "\"\"") ?? "");

                //CloudWatch log message
                if (LogLevel == 1 || LogLevel == 2)
                {
                    Tracer.AddLogEvent(new TracerParameter(ErrorMessage, "", System.Environment.MachineName, "", "", true, -1));
                    Tracer.Dispose();
                }

                ErrorLog.AccumulatedLogMessage += "\"" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "\",\"GoogleAnalytics\",\"" + (Account.AccountID ?? "") + "\",\"\",\"\",\"An error occurred getting profiles list = " + (e2.Message.Replace("\"", "\"\"") ?? "") + "\", \"TPMWorkerGA\"\r\n";
                ErrorLog.ReportError(ConfigurationManager.AppSettings["LogFileErrorName"], (Account.AccountID ?? ""), "", "TPMWorkerGA");
                Environment.Exit(14);
            }

            return;
        }

        private static Boolean CheckGAMetricsDimensions()
        {
            string GAMetrics = ConfigurationManager.AppSettings["GAMetrics"];
            string[] GAMetricsArray = GAMetrics.Split(',');
            string GADimensions = ConfigurationManager.AppSettings["GADimensions"];
            string[] GADimensionsArray = GADimensions.Split(',');

            if (Array.IndexOf(GAMetricsArray, "ga:totalEvents") < 0 || Array.IndexOf(GAMetricsArray, "ga:sessions") < 0 ||
                Array.IndexOf(GADimensionsArray, "ga:date") < 0 || Array.IndexOf(GADimensionsArray, "ga:eventAction") < 0 ||
                Array.IndexOf(GADimensionsArray, "ga:eventLabel") < 0 || Array.IndexOf(GADimensionsArray, "ga:landingPagePath") < 0
                )
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }
}

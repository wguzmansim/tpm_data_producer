﻿using System;
using System.Collections.Generic;
using RestSharp;
using System.Configuration;
using Google.Apis.Auth.OAuth2;
using Newtonsoft.Json;
using System.IO;
using TPMEntities;
using TPMLog;
using VelocityTracer;
//using Google.Apis.MyBusiness.v4.Data;
using RestSharp.Deserializers;

namespace TPMWorkerGMB
{
    public class GMBMetrics
    {
        int LogLevel = int.Parse(ConfigurationManager.AppSettings["LogLevel"]);
        public string LogMessage = "";
        public string ErrorMessage = "";

        public void GetAllMetrics(ProfileData ProfileData, Log ErrorLog, Tracer Tracer)
        {
            //CloudWatch log message
            if (LogLevel == 2)
            {
                LogMessage = string.Format("TPMWorkerGMB getting metrics data for profile {0}", ProfileData.ID);
                Tracer.AddLogEvent(new TracerParameter(LogMessage, "", System.Environment.MachineName, "", "", false, -1));
            }

            string GMBPrivateKey = ConfigurationManager.AppSettings["GMBPrivateKey"].Replace(@"\n", "\n");
            var GMBServiceAccountEmail = ConfigurationManager.AppSettings["GMBServiceAccountEmail"];
            var GMBUserAccountEmail = ConfigurationManager.AppSettings["GMBUserAccountEmail"];

            //Google Authentication
            ServiceAccountCredential credential = new ServiceAccountCredential(
                new ServiceAccountCredential.Initializer(GMBServiceAccountEmail)
                {
                    Scopes = new[] { ConfigurationManager.AppSettings["GMBScope"] },
                    User = GMBUserAccountEmail

                }.FromPrivateKey(GMBPrivateKey));

            //Gets access token
            string AccessToken = "";
            try
            {
                //CloudWatch log message
                if (LogLevel == 2)
                {
                    LogMessage = string.Format("TPMWorkerGMB generating access token for profile {0}", ProfileData.ID);
                    Tracer.AddLogEvent(new TracerParameter(LogMessage, "", System.Environment.MachineName, "", "", false, -1));
                }

                AccessToken = credential.GetAccessTokenForRequestAsync().Result;
            }
            catch (Exception e)
            {
                ErrorMessage = string.Format("TPMWorkerGMB generated an error while creating access token for profile {0}, error = {1}", ProfileData.ID, e.Message.Replace("\"", "\"\""));

                //CloudWatch log message
                if (LogLevel == 1 || LogLevel == 2)
                {
                    Tracer.AddLogEvent(new TracerParameter(ErrorMessage, "", System.Environment.MachineName, "", "", true, -1));
                    Tracer.Dispose();
                }

                ErrorLog.AccumulatedLogMessage += "\"" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "\",\"GoogleMyBusiness\",\"" + (ProfileData.DirectoryID ?? "") + "\",\"" + (ProfileData.ID ?? "") + "\",\"\",\"An error occurred trying to generate Google Access Token = " + (e.Message.Replace("\"", "\"\"") ?? "") + "\", \"TPMWorkerGMB\"\r\n";
                ErrorLog.ReportError(ConfigurationManager.AppSettings["LogFileErrorName"], (ProfileData.DirectoryID ?? ""), (ProfileData.ID ?? ""), "TPMWorkerGMB");
                Environment.Exit(5);
            }

            //Creates the service.
            /*
            var Service = new Google.Apis.MyBusiness.v3.MyBusinessService(new Google.Apis.Services.BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = "SIM Google My Business API",
            });
            

            var Service = new Google.Apis.MyBusiness.v4.MyBusinessService(new Google.Apis.Services.BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = "SIM Google My Business API",
            });
            */

            RestRequest request = new RestRequest(Method.POST);
            RestClient client = new RestClient();
            string json = "";
            try
            {
                //CloudWatch log message
                if (LogLevel == 2)
                {
                    Tracer.AddLogEvent(new TracerParameter(string.Format("TPMWorkerGMB is making a request for profile {0}", ProfileData.ID), "", System.Environment.MachineName, "", "", false, -1));
                }

                var GMBAccountID = ProfileData.ThirdPartyProfileID.Split('/')[1];
                var AccountName = "accounts/" + GMBAccountID;
                string InsightsUrl = string.Format(ConfigurationManager.AppSettings["GMBInsightsURL"], GMBAccountID);

                //Gets Insights for each location
                client.BaseUrl = new Uri(InsightsUrl);
                List<GMBMetricRequest> MetricRequests = new List<GMBMetricRequest>();
                MetricRequests.Add(new GMBMetricRequest()
                {
                    metric = "ALL",
                    options = "AGGREGATED_DAILY"
                });

                GMBBasicRequest BasicRequest = new GMBBasicRequest();
                BasicRequest.metricRequests = MetricRequests;

                GMBTimeRange TimeRangeRequest = new GMBTimeRange();
                TimeRangeRequest.endTime = ProfileData.EndDateMetrics.ToString("s") + "Z";
                //Start date provided by SQS message
                TimeRangeRequest.startTime = ProfileData.StartDateMetrics.ToString("s") + "Z";
                //Start date calculated (-7 days current date)
                //TimeRangeRequest.startTime = ProfileData.EndDateMetrics.AddDays(-7).ToString("s") + "Z";

                BasicRequest.timeRange = TimeRangeRequest;

                GMBRequest FullRequest = new GMBRequest();
                FullRequest.basicRequest = BasicRequest;

                List<string> LocationNamesList = new List<string>();
                LocationNamesList.Add(ProfileData.ThirdPartyProfileID);
                FullRequest.locationNames = LocationNamesList;
                json = JsonConvert.SerializeObject(FullRequest);

                //CloudWatch log message
                if (LogLevel == 2)
                {
                    Tracer.AddLogEvent(new TracerParameter(string.Format("TPMWorkerGMB API request sent {1} for profile {0}", ProfileData.ID, json), "", System.Environment.MachineName, "", "", true, -1));
                }
            }
            catch (Exception e)
            {
                ErrorMessage = string.Format("TPMWorkerGMB failed to create request for GMB API for profile {0}, error = {1}", ProfileData.ID, e.Message.Replace("\"", "\"\""));

                //CloudWatch log message
                if (LogLevel == 1 || LogLevel == 2)
                {
                    Tracer.AddLogEvent(new TracerParameter(ErrorMessage, "", System.Environment.MachineName, "", "", true, -1));
                    Tracer.Dispose();
                }

                Log.LogMessage(ErrorMessage, ConfigurationManager.AppSettings["LogFileName"]);

                //Keeps error log to send to ETL temporary table
                ErrorLog.AccumulatedLogMessage += "\"" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "\",\"GoogleMyBusiness\",\"" + (ProfileData.DirectoryID ?? "") + "\",\"" + (ProfileData.ID ?? "") + "\",\"\",\"TPMWorkerGMB failed to create request for GMB API = " + e.Message.Replace("\"", "\"\"") + "\", \"TPMWorkerGMB\"\r\n";
                ErrorLog.ReportError(ConfigurationManager.AppSettings["LogFileErrorName"], (ProfileData.DirectoryID ?? ""), (ProfileData.ID ?? ""), "TPMWorkerGMB");
                Environment.Exit(6);
            }

            request.AddParameter("Application/Json", json, ParameterType.RequestBody);
            request.RequestFormat = DataFormat.Json;
            request.AddHeader("Authorization", "Bearer " + credential.Token.AccessToken);
            request.AddHeader("X-GOOG-API-FORMAT-VERSION", "4");
            request.AddHeader("Content-type", "application/json");
            RestResponse response = null;
            try
            {
                response = (RestResponse)client.Execute(request);

                //CloudWatch log message
                if (LogLevel == 2)
                {
                    Tracer.AddLogEvent(new TracerParameter("TPMWorkerGMB API response received = " + response.Content, "", System.Environment.MachineName, "", "", false, -1));
                }
            }
            catch (Exception e)
            {
                ErrorMessage = string.Format("TPMWorkerGMB generated an error from response for profile {0}, error = {1}", ProfileData.ID, e.InnerException.Message.Replace("\"", "\"\""));
                Log.LogMessage(ErrorMessage, ConfigurationManager.AppSettings["LogFileName"]);

                //CloudWatch log message
                if (LogLevel == 1 || LogLevel == 2)
                {
                    Tracer.AddLogEvent(new TracerParameter(ErrorMessage, "", System.Environment.MachineName, "", "", true, -1));
                }

                ErrorLog.AccumulatedLogMessage += "\"" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "\",\"GoogleMyBusiness\",\"" + (ProfileData.DirectoryID ?? "") + "\",\"" + (ProfileData.ID ?? "") + "\",\"\",\"An error occurred getting response from GMB API = " + e.InnerException.Message.Replace("\"", "\"\"") + "\", \"TPMWorkerGMB\"\r\n";
            }

            if (response.Content == "" || response.Content == "{}\n")
            {
                ErrorMessage = string.Format("TPMWorkergGMB got a null response for profile {0}", ProfileData.ID);
                Log.LogMessage(ErrorMessage, ConfigurationManager.AppSettings["LogFileName"]);
                ErrorLog.AccumulatedLogMessage += "\"" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "\",\"GoogleMyBusiness\",\"" + (ProfileData.DirectoryID ?? "") + "\",\"" + (ProfileData.ID ?? "") + "\",\"\",\"Null response content from GMB API\", \"TPMWorkerGMB\"\r\n";
                ErrorLog.ReportError(ConfigurationManager.AppSettings["LogFileErrorName"], (ProfileData.DirectoryID ?? ""), (ProfileData.ID ?? ""), "TPMWorkerGMB");

                //CloudWatch log message
                if (LogLevel == 1 || LogLevel == 2)
                {
                    Tracer.AddLogEvent(new TracerParameter(ErrorMessage, "", System.Environment.MachineName, "", "", true, -1));
                    Tracer.Dispose();
                }

                Environment.Exit(18);
            }

            //Checks if response had a bad request
            if (response.ResponseStatus.ToString() == "Error")
            {
                ErrorMessage = string.Format("TPMWorkerGMB generated an error in GMB API response = {0}", response.ErrorMessage.Replace("\"", "\"\""));

                //CloudWatch log message
                if (LogLevel == 1 || LogLevel == 2)
                {
                    Tracer.AddLogEvent(new TracerParameter(ErrorMessage, "", System.Environment.MachineName, "", "", true, -1));
                    Tracer.Dispose();
                }

                ErrorLog.AccumulatedLogMessage += "\"" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "\",\"GoogleMyBusiness\",\"" + (ProfileData.DirectoryID ?? "") + "\",\"" + (ProfileData.ID ?? "") + "\",\"\",\"Error in GMB API response = " + response.ErrorMessage.Replace("\"", "\"\"") + "\", \"TPMWorkerGMB\"\r\n";
                ErrorLog.ReportError(ConfigurationManager.AppSettings["LogFileErrorName"], (ProfileData.DirectoryID ?? ""), (ProfileData.ID ?? ""), "TPMWorkerGMB");
                Environment.Exit(6);
            }

            //Checks if response code error for a deleted entity
            try
            {
                GMBResponse GMBTest = JsonConvert.DeserializeObject<GMBResponse>(response.Content);
                int GMBLocationsTotal = GMBTest.locationMetrics.Count;
            }
            catch (Exception e)
            {
                ErrorMessage = string.Format("TPMWorkerGMB got a deleted entity, message = {0}", e.Message);
                //CloudWatch log message
                if (LogLevel == 1 || LogLevel == 2)
                {
                    Tracer.AddLogEvent(new TracerParameter(ErrorMessage, "", System.Environment.MachineName, "", "", true, -1));
                    Tracer.Dispose();
                }

                ErrorLog.AccumulatedLogMessage += "\"" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "\",\"GoogleMyBusiness\",\"" + (ProfileData.DirectoryID ?? "") + "\",\"" + (ProfileData.ID ?? "") + "\",\"\",\"Deleted entity\", \"TPMWorkerGMB\"\r\n";
                ErrorLog.ReportError(ConfigurationManager.AppSettings["LogFileErrorName"], (ProfileData.DirectoryID ?? ""), (ProfileData.ID ?? ""), "TPMWorkerGMB");
                Environment.Exit(8);
            }

            //Prepares contents of CSV file
            //Saves CSV to local file
            string FileName = "TPMWorkerGMB_" + ProfileData.ID + "_" + DateTime.Now.ToString("yyyy-MM-dd_HH_mm_ss") + ".csv";
            FileName = FileName.Replace("/", "_");
            string TemporaryFilename = ConfigurationManager.AppSettings["CSVFolder"] + "\\" + FileName;
            this.GMBCreateCSVString(ProfileData, response.Content, TemporaryFilename, ErrorLog, Tracer);
            var KeepCSVFiles = bool.Parse(ConfigurationManager.AppSettings["KeepTemporaryCSVFile"]);

            //Saves local file in S3 bucket
            string BucketName = ConfigurationManager.AppSettings["S3BucketPath"];
            Boolean SaveAttempt = false;
            try
            {
                SIMAmazon AmazonBucket = new SIMAmazon();
                var S3Attempts = 0;
                var S3MaxAttempts = 5;
                while (S3Attempts <  S3MaxAttempts && !SaveAttempt)
                {
                    S3Attempts++;
                    try
                    {
                        SaveAttempt = AmazonBucket.SaveToS3(BucketName, TemporaryFilename, DateTime.Now.ToString("yyyy") + "/" + DateTime.Now.ToString("MM") + "/" + DateTime.Now.ToString("dd") + "/" + FileName, ProfileData.ProcessStartDate, ProfileData.DirectoryID, ProfileData.ID, ErrorLog);
                    }
                    catch (Exception e)
                    {
                        if (S3Attempts >= S3MaxAttempts)
                        {
                            throw;
                        }
                        System.Threading.Thread.Sleep(1000);
                    }
                }
                if (!KeepCSVFiles)
                {
                    File.Delete(TemporaryFilename);
                }
            }
            catch (Exception e)
            {
                ErrorMessage = string.Format("TPMWorkergGMB generated an error creating the S3 bucket file = {0}", (e.Message.Replace("\"", "\"\"") ?? ""));

                //CloudWatch log message
                if (LogLevel == 1 || LogLevel == 2)
                {
                    Tracer.AddLogEvent(new TracerParameter(ErrorMessage, "", System.Environment.MachineName, "", "", true, -1));
                    Tracer.Dispose();
                }

                ErrorLog.AccumulatedLogMessage += "\"" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "\",\"GoogleMyBusiness\",\"" + (ProfileData.DirectoryID ?? "") + "\",\"" + (ProfileData.ID ?? "") + "\",\"\",\"Error creating the S3 bucket file = " + (e.Message.Replace("\"", "\"\"") ?? "") + "\", \"TPMWorkerGMB\"\r\n";
                ErrorLog.ReportError(ConfigurationManager.AppSettings["LogFileErrorName"], (ProfileData.DirectoryID ?? ""), (ProfileData.ID ?? ""), "TPMWorkerGMB");

                //Saves failed file in local folder
                if (!Directory.Exists(ConfigurationManager.AppSettings["CSVFolder"] + "\\" + ConfigurationManager.AppSettings["CSVFailedUploadsFolder"] + "\\"))
                {
                    Directory.CreateDirectory(ConfigurationManager.AppSettings["CSVFolder"] + "\\" + ConfigurationManager.AppSettings["CSVFailedUploadsFolder"] + "\\");
                }
                File.Move(TemporaryFilename, ConfigurationManager.AppSettings["CSVFolder"] + "\\" + ConfigurationManager.AppSettings["CSVFailedUploadsFolder"] + "\\" + FileName);
                Environment.Exit(9);
            }

            ErrorLog.ReportError(ConfigurationManager.AppSettings["LogFileErrorName"], (ProfileData.DirectoryID ?? ""), "", "TPMWorkerGMB");

            //CloudWatch log message
            if (LogLevel == 2)
            {
                LogMessage = string.Format("TPMWorkerGMB finished metrics data for DirectoryID = {0}, ProfileID = {1}", (ProfileData.DirectoryID ?? ""), (ProfileData.ID ?? ""));
                Tracer.AddLogEvent(new TracerParameter(LogMessage, "", System.Environment.MachineName, "", "", false, -1));
                Tracer.Dispose();
            }

            Environment.Exit(0);
            return;
        }

        public void GMBCreateCSVString(ProfileData ProfileInfo, string JSONString, string LocalFileName, Log ErrorLog, Tracer Tracer)
        {
            string CSVContents = "";
            string CSVHeaders = "Date, DirectoryID, DirectoryName, MetricName, MetricValue, ProfileAddress1, ProfileAddress2, ProfileBusinessType, ProfileCity, ProfileID, ProfileLatitude, ProfileLongitude, ProfileName, ProfilePhone, ProfileState, ProfileWebsite, ProfileZipCode, ThirdPartyChannelName, ThirdPartyProfileID, ThirdPartyURL";
            File.AppendAllLines(LocalFileName, new List<string>() { CSVHeaders });

            string CurrentDate = DateTime.Now.ToString("yyyy-MM-dd 00:00:00");
            GMBResponse GMBResponseObject = new GMBResponse();
            try
            {
                GMBResponseObject = JsonConvert.DeserializeObject<GMBResponse>(JSONString);
            }
            catch (Exception e)
            {
                ErrorMessage = string.Format("TPMWorkerGMB generated an error, got invalid JSON response for profile {0}", ProfileInfo.ID);
                Log.LogMessage(ErrorMessage, ConfigurationManager.AppSettings["LogFileName"]);
                ErrorLog.AccumulatedLogMessage += "\"" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "\",\"GoogleMyBusiness\",\"" + (ProfileInfo.DirectoryID ?? "") + "\",\"" + (ProfileInfo.ID ?? "") + "\",\"\",\"Invalid JSON response from GMB API = " + (e.Message.Replace("\"", "\"\"") ?? "") + "\", \"TPMWorkerGMB\"" + Environment.NewLine;
                ErrorLog.ReportError(ConfigurationManager.AppSettings["LogFileErrorName"], (ProfileInfo.DirectoryID ?? ""), (ProfileInfo.ID ?? ""), "TPMWorkerGMB");

                //CloudWatch log message
                if (LogLevel == 1 || LogLevel == 2)
                {
                    Tracer.AddLogEvent(new TracerParameter(ErrorMessage, "", System.Environment.MachineName, "", "", true, -1));
                    Tracer.Dispose();
                }

                return;
            }
            if (GMBResponseObject.locationMetrics == null)
            {
                ErrorMessage = string.Format("TPMWorkerGMB generated an error, got location metrics null for profile {0}", ProfileInfo.ID);
                Log.LogMessage(ErrorMessage, ConfigurationManager.AppSettings["LogFileName"]);

                //CloudWatch log message
                if (LogLevel == 1 || LogLevel == 2)
                {
                    Tracer.AddLogEvent(new TracerParameter(ErrorMessage, "", System.Environment.MachineName, "", "", true, -1));
                    Tracer.Dispose();
                }

                ErrorLog.AccumulatedLogMessage += "\"" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "\",\"GoogleMyBusiness\",\"" + (ProfileInfo.DirectoryID ?? "") + "\",\"" + (ProfileInfo.ID ?? "") + "\",\"\",\"Location metrics null\", \"TPMWorkerGMB\"\r\n";
                ErrorLog.ReportError(ConfigurationManager.AppSettings["LogFileErrorName"], (ProfileInfo.DirectoryID ?? ""), (ProfileInfo.ID ?? ""), "TPMWorkerGMB");
                return;
            }
            foreach (GMBResponseLocationMetric LocationMetric in GMBResponseObject.locationMetrics)
            {
                foreach (GMBResponseMetricValue MetricValue in LocationMetric.metricValues)
                {
                    foreach (GMBResponseDimensionalValue DimensionalValue in MetricValue.dimensionalValues)
                    {
                        string OriginalFormatDate = DimensionalValue.timeDimension.timeRange.startTime;
                        DateTime DateFormatted = Convert.ToDateTime(OriginalFormatDate);
                        string NewFormatDate = DateFormatted.ToString("yyyy-MM-dd 00:00:00");
                        CSVContents =
                                    "\"" + NewFormatDate + "\"," +
                                    "\"" + ProfileInfo.DirectoryID + "\"," +
                                    "\"" + ProfileInfo.DirectoryName.Replace("\"", "\"\"") + "\"," +
                                    "\"" + MetricValue.metric + "\"," +
                                    "\"" + DimensionalValue.value + "\"," +
                                    "\"" + ProfileInfo.Address1.Replace("\"", "\"\"") + "\"," +
                                    "\"" + ProfileInfo.Address2.Replace("\"", "\"\"") + "\"," +
                                    "\"" + ProfileInfo.BusinessType.Replace("\"", "\"\"") + "\"," +
                                    "\"" + ProfileInfo.City.Replace("\"", "\"\"") + "\"," +
                                    "\"" + ProfileInfo.ID.Replace("\"", "\"\"") + "\"," +
                                    "\"" + ProfileInfo.Latitude + "\"," +
                                    "\"" + ProfileInfo.Longitude + "\"," +
                                    "\"" + ProfileInfo.Name.Replace("\"", "\"\"") + "\"," +
                                    "\"" + ProfileInfo.Phone.Replace("\"", "\"\"") + "\"," +
                                    "\"" + ProfileInfo.State.Replace("\"", "\"\"") + "\"," +
                                    "\"" + ProfileInfo.Website + "\"," +
                                    "\"" + ProfileInfo.ZipCode.Replace("\"", "\"\"") + "\"," +
                                    "\"" + ProfileInfo.ThirdPartyChannelName + "\"," +
                                    "\"" + ProfileInfo.ThirdPartyProfileID.Replace("\"", "\"\"") + "\"," +
                                    "\"" + ProfileInfo.ThirdPartyURL + "\"";

                        File.AppendAllLines(LocalFileName, new List<string>() { CSVContents });
                    }
                }
            }

            return;
        }
    }

    public class GMBRequest
    {
        public GMBBasicRequest basicRequest { get; set; }
        public List<string> locationNames { get; set; }
    }

    public class GMBBasicRequest
    {
        public List<GMBMetricRequest> metricRequests { get; set; }
        public GMBTimeRange timeRange { get; set; }
    }

    public class GMBMetricRequest
    {
        public string metric { get; set; }
        public string options { get; set; }
    }

    public class GMBTimeRange
    {
        public string endTime { get; set; }
        public string startTime { get; set; }
    }

    //Classes related to the GMB response JSON object
    public class GMBResponseTimeRange
    {
        public string startTime { get; set; }
        public string endTime { get; set; }
    }

    public class GMBResponseTimeDimension
    {
        public GMBResponseTimeRange timeRange { get; set; }
    }

    public class GMBResponseTotalValue
    {
        public string metricOption { get; set; }
        public GMBResponseTimeDimension timeDimension { get; set; }
        public string value { get; set; }
    }

    public class GMBResponseDimensionalValue
    {
        public string metricOption { get; set; }
        public GMBResponseTimeDimension timeDimension { get; set; }
        public string value { get; set; }
    }

    public class GMBResponseMetricValue
    {
        public string metric { get; set; }
        public GMBResponseTotalValue totalValue { get; set; }
        public List<GMBResponseDimensionalValue> dimensionalValues { get; set; }
    }

    public class GMBResponseLocationMetric
    {
        public string locationName { get; set; }
        public string timeZone { get; set; }
        public List<GMBResponseMetricValue> metricValues { get; set; }
    }

    public class GMBResponse
    {
        public List<GMBResponseLocationMetric> locationMetrics { get; set; }
    }
}

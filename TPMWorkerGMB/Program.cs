﻿using System;
using System.Configuration;
using TPMLog;
using Newtonsoft.Json;
using TPMEntities;
using VelocityTracer;
using System.Net;

namespace TPMWorkerGMB
{
    class Program
    {
        static void Main(string[] args)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;

            Tracer Tracer = new Tracer("TPMWorkerGMB");
            int LogLevel = int.Parse(ConfigurationManager.AppSettings["LogLevel"]);
            var ProfileData = new ProfileData();
            Log ErrorLog = new Log();

            try
            {
                //CloudWatch log message
                if (LogLevel == 2)
                {
                    Tracer.AddLogEvent(new TracerParameter("TPMWorkerGMB arguments provided = " + String.Join(" ", args).Replace("\"", "\"\""), "", System.Environment.MachineName, "", "", false, -1));
                }

                string TempArgs = String.Join(" ", args).Replace("\"\"", "'");
                TempArgs = TempArgs.Replace(":',", ":\"\",");

                ProfileData = JsonConvert.DeserializeObject<ProfileData>(TempArgs);

                //To make sure we get all required fields from program arguments
                String ProfileID = ProfileData.ID;
                String DirectoryID = ProfileData.DirectoryID;
                String DirectoryName = ProfileData.DirectoryName;
                String Name = ProfileData.Name;
                String Address1 = ProfileData.Address1;
                String BusinessType = ProfileData.BusinessType;
                String City = ProfileData.City;
                String Latitude = ProfileData.Latitude;
                String Longitude = ProfileData.Longitude;
                String Phone = ProfileData.Phone;
                String State = ProfileData.State;
                String ZipCode = ProfileData.ZipCode;
                String ThirdPartyChannel = ProfileData.ThirdPartyChannel.ToString();
                String ThirdPartyProfileID = ProfileData.ThirdPartyProfileID;
                String StartDateMetrics = ProfileData.StartDateMetrics.ToString();
                String EndDateMetrics = ProfileData.EndDateMetrics.ToString();
            }
            catch (Exception e)
            {
                //CloudWatch log message
                if (LogLevel == 1 || LogLevel == 2)
                {
                    Tracer.AddLogEvent(new TracerParameter("Invalid arguments for TPMWorkerGMB, args provided = " + String.Join(" ", args).Replace("\"", "\"\""), "", System.Environment.MachineName, "", "", true, -1));
                    Tracer.Dispose();
                }

                ErrorLog.AccumulatedLogMessage += "\"" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "\",\"GoogleMyBusiness\",\"\",\"\",\"\",\"Invalid arguments for TPMWorkerGMB, args provided = " + String.Join(" ", args).Replace("\"", "\"\"") + ", error message = " + e.Message.Replace("\"", "\"\"") + " \", \"TPMWorkerGMB\"\r\n";
                ErrorLog.ReportError(ConfigurationManager.AppSettings["LogFileErrorName"], "", "", "TPMWorkerGMB");

                Environment.Exit(1);
            }

            GMBMetrics GMBAllMetrics = new GMBMetrics();
            GMBAllMetrics.GetAllMetrics(ProfileData, ErrorLog, Tracer);

            return;
        }
    }
}
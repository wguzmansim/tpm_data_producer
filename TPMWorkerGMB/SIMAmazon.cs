﻿extern alias AWSCore;
extern alias AWSRedshift;

using Amazon.Redshift;
using Amazon.S3;
using Amazon.S3.Model;

using System;
using System.Configuration;
using System.Data.Odbc;
using TPMLog;
using VelocityTracer;

namespace TPMWorkerGMB
{
    public class SIMAmazon
    {
        static IAmazonS3 client = new AmazonS3Client(Amazon.RegionEndpoint.USEast1);
        int LogLevel = int.Parse(ConfigurationManager.AppSettings["LogLevel"]);

        public Boolean SaveToS3(string BucketName, string SourceFilePath, string DestinationFilename, DateTime ProcessStartDate, string DirectoryID, string ProfileID, Log ErrorLog)
        {
            try
            {
                PutObjectRequest putRequest2 = new PutObjectRequest
                {
                    BucketName = BucketName,
                    Key = DestinationFilename,
                    FilePath = SourceFilePath,
                    ContentType = "text/plain"
                };
                putRequest2.Metadata.Add("x-amz-meta-title", "someTitle");

                PutObjectResponse response2 = client.PutObject(putRequest2);

                return true;
            }
            catch (AmazonS3Exception amazonS3Exception)
            {
                if (amazonS3Exception.ErrorCode != null &&
                    (amazonS3Exception.ErrorCode.Equals("InvalidAccessKeyId")
                    ||
                    amazonS3Exception.ErrorCode.Equals("InvalidSecurity")))
                {
                    ErrorLog.AccumulatedLogMessage += "\"" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "\",\"GoogleMyBusiness\",\"" + (DirectoryID ?? "") + "\",\"" + (ProfileID ?? "") + "\",\"\",\"Invalid AWS credentials when trying to upload S3 file = " + (amazonS3Exception.Message.Replace("\"", "\"\"") ?? "") + "\", \"TPMWorkerGMB\"\r\n";
                }
                else
                {
                    ErrorLog.AccumulatedLogMessage += "\"" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "\",\"GoogleMyBusiness\",\"" + (DirectoryID ?? "") + "\",\"" + (ProfileID ?? "") + "\",\"\",\"Error occurred when trying to upload S3 file = " + (amazonS3Exception.Message.Replace("\"", "\"\"") ?? "") + "\", \"TPMWorkerGMB\"\r\n";
                }

                return false;
            }
        }

        public void LoadCSVToRedshift(string FileToLoad, Log ErrorLog, Tracer Tracer)
        {
            String mainDbName = ConfigurationManager.AppSettings["RedshiftDBName"];
            String endpointAddress = ConfigurationManager.AppSettings["RedshiftHost"];
            String endpointPort = ConfigurationManager.AppSettings["RedshiftPort"];
            string masterUsername = ConfigurationManager.AppSettings["RedshiftUsername"];
            string password = ConfigurationManager.AppSettings["RedshiftPassword"];

            string odbcConnectionString = string.Concat("Driver={PostgreSQL Unicode}; Server=", endpointAddress, "; Database=", mainDbName, "; UID=", masterUsername, "; PWD=", password, "; Port=", endpointPort);
            using (OdbcConnection conn = new OdbcConnection(odbcConnectionString))
            {
                try
                {
                    conn.Open();
                    //Load the CSV file into the temporary table
                    OdbcCommand CommandCopy = new OdbcCommand("COPY etl.etl_tpm_temp_metric_data_bulkload (etl_date, etl_directory_id, etl_directory_name, etl_metric_name, etl_metric_value, etl_profile_address_1, etl_profile_address_2, etl_profile_business_type, etl_profile_city, etl_profile_id, etl_profile_latitude, etl_profile_longitude, etl_profile_name, etl_profile_phone, etl_profile_state, etl_profile_website, etl_profile_zip_code, etl_third_party_name, etl_third_party_profile_id, etl_third_party_url) FROM '" + FileToLoad + "' WITH CREDENTIALS 'aws_access_key_id=AKIAIPSFT4GVHCUNH5XQ;aws_secret_access_key=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' REGION 'us-west-2' IGNOREHEADER 1 CSV DELIMITER ',';", conn);
                    CommandCopy.ExecuteNonQuery();
                }
                catch (AmazonRedshiftException e)
                {
                    if (e.ErrorCode != null &&
                        (e.ErrorCode.Equals("InvalidAccessKeyId")
                        ||
                        e.ErrorCode.Equals("InvalidSecurity")))
                    {
                        //CloudWatch log message
                        if (LogLevel == 1 || LogLevel == 2)
                        {
                            string ErrorMessage = string.Format("Invalid AWS credentials when trying to upload S3 file = " + (e.Message.Replace("\"", "\"\"") ?? ""));

                            Tracer.AddLogEvent(new TracerParameter(ErrorMessage, "", System.Environment.MachineName, "", "", true, -1));
                        }

                        ErrorLog.AccumulatedLogMessage += "\"" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "\",\"GoogleMyBusiness\",\"\",\"\",\"\",\"Invalid AWS credentials when trying to upload S3 file = " + (e.Message.Replace("\"", "\"\"") ?? "") + "\", \"TPMWorkerGMB\"\r\n";
                    }
                    else
                    {
                        //CloudWatch log message
                        if (LogLevel == 1 || LogLevel == 2)
                        {
                            string ErrorMessage = string.Format("Error occured when trying to upload S3 file = " + (e.Message.Replace("\"", "\"\"") ?? ""));

                            Tracer.AddLogEvent(new TracerParameter(ErrorMessage, "", System.Environment.MachineName, "", "", true, -1));
                        }

                        ErrorLog.AccumulatedLogMessage += "\"" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "\",\"GoogleMyBusiness\",\"\",\"\",\"\",\"Error occured when trying to upload S3 file = " + (e.Message.Replace("\"", "\"\"") ?? "") + "\", \"TPMWorkerGMB\"\r\n";
                    }
                }
            }
        }
    }
}
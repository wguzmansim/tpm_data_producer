﻿using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace TPMWorkerGMB
{
    public class VelocityProfile
    {
        public string VelocityDirectoryID { get; set; }
        public string VelocityDirectoryName { get; set; }
        public string VelocityProfileID { get; set; }
        public string VelocityProfileName { get; set; }
        public string VelocityProfileAddress { get; set; }
        public string VelocityProfileBusinessType { get; set; }
        public string VelocityProfileCity { get; set; }
        public string VelocityProfileLatitude { get; set; }
        public string VelocityProfileLongitude { get; set; }
        public string VelocityProfilePhone { get; set; }
        public string VelocityProfileState { get; set; }
        public string VelocityProfileWebsite { get; set; }
        public string VelocityProfileZipCode { get; set; }
        public VelocityProfile GetVelocityProfileInfo(string ProfileID)
        {
            //Connects to database
            string ConnectionString = ConfigurationManager.ConnectionStrings["VelocityDatabase"].ConnectionString;
            //var Con = new SqlConnection(ConnectionString);
            //var Cmd = new SqlCommand("", Con);
            var DA = new SqlDataAdapter("pro_GetData", ConnectionString);
            DA.SelectCommand.CommandType = CommandType.StoredProcedure;
            DA.SelectCommand.Parameters.AddWithValue("@pro_ID", ProfileID);
            var Table = new DataSet();
            DA.Fill(Table);

            VelocityProfile ProfileInfo = new VelocityProfile();
            ProfileInfo.VelocityDirectoryID = Table.Tables[0].Rows[0]["pts_ID"].ToString();
            ProfileInfo.VelocityDirectoryName = Table.Tables[0].Rows[0]["pts_Name"].ToString();
            ProfileInfo.VelocityProfileID = ProfileID.ToString();
            ProfileInfo.VelocityProfileName = Table.Tables[0].Rows[0]["pro_BusinessName"].ToString();
            ProfileInfo.VelocityProfileAddress = Table.Tables[0].Rows[0]["pro_AddressLine1"].ToString() + " " + Table.Tables[0].Rows[0]["pro_AddressLine2"].ToString();
            ProfileInfo.VelocityProfileBusinessType = Table.Tables[0].Rows[0]["pro_BusinessType"].ToString();
            ProfileInfo.VelocityProfileCity = Table.Tables[0].Rows[0]["pro_City"].ToString();
            ProfileInfo.VelocityProfileLatitude = Table.Tables[0].Rows[0]["pro_Latitude"].ToString();
            ProfileInfo.VelocityProfileLongitude = Table.Tables[0].Rows[0]["pro_Longitude"].ToString();
            ProfileInfo.VelocityProfilePhone = Table.Tables[0].Rows[0]["pro_Phone"].ToString();
            ProfileInfo.VelocityProfileState = Table.Tables[0].Rows[0]["pro_State"].ToString();
            ProfileInfo.VelocityProfileWebsite = Table.Tables[0].Rows[0]["pro_ProfileWebAddress"].ToString();
            ProfileInfo.VelocityProfileZipCode = Table.Tables[0].Rows[0]["pro_ZipCode"].ToString();
            return ProfileInfo;
        }
    }
}
